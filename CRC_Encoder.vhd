library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--Function takes in data and ID and applies CAN protocol
--need to implement part that checks whether a high priority node is using the bus (I.E. check for a 1 on the bus)


entity CRC_Encoder is
    port 
    (
		i_data 		: in std_logic_vector(7 downto 0);
		i_clk			: in std_logic;
		o_CRC_Code  : out std_logic_vector(17 downto 0)
    );
end CRC_Encoder;


architecture behaviour of CRC_Encoder is
begin

process(i_clk)
 variable r_code 			:std_logic_vector(17 downto 0); -- to hold the 8 data bits plus padded zeros
 variable r_CRC_Poly		:std_logic_vector(10 downto 0) := "11001001111"; -- Polynomial used to check and encode
 variable w		  			:std_logic_vector(10 downto 0);
 variable y		 			:std_logic_vector(10 downto 0);
 variable i,j    			:integer:=0;
 
 begin
 r_code(17 downto 10):=i_data(7 downto 0); -- store message bits followed bt (n-1) 0's

 for j in 9 downto 0 loop -- add 10 zeros to the data bits
	r_code(j):='0';
 end loop;
 
 w := r_code(17 downto 7); -- take the first 11 bits of r_Code
 
 for i in 7 downto 0 loop -- if msb of w is 1, xor that ish with the polynomial
		if(w(10)='1') then
			w:=w xor r_CRC_Poly; -- xor the first 11 bits of r_Code with the crc polynomials
		else
			null;
		end if;
		
		if(i > 0) then
			y:=w; -- y gets the result
			
			w(10 downto 1):=y(9 downto 0); --shift once to the left
			
			if(i=0) then
				w(0):='0';
			else
				w(0):=r_code(i-1); -- fill in the last bit with the next bit in r_Code
			end if;
		end if;
 end loop;
 
 o_CRC_Code(17 downto 10)<=i_data; -- first 8 bits is the original data
 o_CRC_Code(9 downto 0)<=w(9 downto 0); -- last 10 bits is the remainder of the xor's
 end process;

end behaviour;
