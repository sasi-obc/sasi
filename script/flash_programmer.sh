#!/bin/sh
#
# This file was automatically generated.
#
# It can be overwritten by nios2-flash-programmer-generate or nios2-flash-programmer-gui.
#

#
# Converting ELF File: C:\Users\Thomas\Documents\GIT\sasi\software\software\software.elf to: "..\flash/software_epcs_controller.flash"
#
elf2flash --input="C:/Users/Thomas/Documents/GIT/sasi/software/software/software.elf" --output="../flash/software_epcs_controller.flash" --epcs --verbose 

#
# Programming File: "..\flash/software_epcs_controller.flash" To Device: epcs_controller
#
nios2-flash-programmer "../flash/software_epcs_controller.flash" --base=0x11000 --epcs --sidp=0x12100 --id=0x0 --timestamp=1582405172 --device=1 --instance=0 '--cable=USB-Blaster on localhost [USB-0]' --program --verbose 

