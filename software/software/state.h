#ifndef STATE_H_
#define STATE_H_

#include <types.h>

/* Comment out to skip display */
#define DISPLAY

typedef void (*StatePointer)();

typedef enum StateName_e {
     STATE_INIT,
     STATE_IDLE,
     STATE_CAPTURE,
     STATE_COMMS,
     STATE_OVERHEATING,
     STATE_DISPLAY
} StateName;

typedef struct _State {
     StatePointer EnterHandler;
     StatePointer ProcessHandler;
     StatePointer ExitHandler;
} State;

typedef struct _SystemState {
     State state;
     uint8_t image_data_valid;
     uint16_t image_line_length;
     uint16_t image_size;
     uint16_t temperature;
} SystemState;

void SystemStateInit(SystemState **systemState);
void ChangeState(SystemState *systemState, StateName newState);

#endif