#ifndef TYPES_H_
#define TYPES_H_

#include <alt_types.h>


typedef alt_8 int8_t;
typedef alt_u8 uint8_t;
typedef alt_16 int16_t;
typedef alt_u16 uint16_t;
typedef alt_32 int32_t;
typedef alt_u32 uint32_t;
typedef alt_64 int64_t;
typedef alt_u64 uint64_t;

#endif