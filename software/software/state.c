#include <state.h>
#include <state_init.h>
#include <state_idle.h>
#include <state_capture.h>
#include <state_comms.h>
#include <state_overheating.h>
#include <state_display.h>

SystemState GlobalSystemState;

State StateTable[] = {  
/***  EnterHandler                    ProcessHandler                    ExitHander              ***/
     {0,                              InitProcessHandler,           0                     },
     {0,                              IdleProcessHandler,           0                     },
     {CaptureEnterHandler,            CaptureProcessHandler,        CaptureExitHandler    },
     {0,                              CommsProcessHandler,          CommsExitHandler      },
     {OverheatingEnterHandler,        OverheatingProcessHandler,    OverheatingExitHandler},
     {0,                              DisplayProcessHandler,        0                     }
};

void SystemStateInit(SystemState **systemState) {
     *systemState = &GlobalSystemState;
}

void ChangeState(SystemState *systemState, StateName newState) {
     if(systemState->state.ExitHandler)
          systemState->state.ExitHandler(systemState);

     systemState->state = StateTable[newState];

     if(systemState->state.EnterHandler)
          systemState->state.EnterHandler(systemState);
}
