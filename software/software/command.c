#include <command.h>
#include <can.h>

uint8_t ReceiveCmd() {
    return CANReceive();
}

uint8_t SendCmd(SASISendCmd_t cmd) {
    CANTransmit(cmd);
    return 1;
}

uint8_t checkMailbox() {
    volatile uint32_t *can_base = (uint32_t *)CAN_REG_BASE;
    return *can_base & CAN_RX_FULL;
}