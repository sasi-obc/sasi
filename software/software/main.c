#include <state.h>
#include <system.h>
#include <types.h>

SystemState *systemState;

int main() {
	SystemStateInit(&systemState);
	ChangeState(systemState, STATE_INIT);
	
	while(1) {
		systemState->state.ProcessHandler(systemState);
	}
	
	return 0;
}
