#ifndef COMMAND_H_
#define COMMAND_H_

#include <types.h>

#define MAILBOX_EMPTY 0xFF
#define OVERHEATING 0x01

typedef enum SASISendCmd {
     IMAGE_CAPTURED      = 0x69,
     IMAGE_CAPTURE_FAIL  = 0xAA,
     COMMS_XFER_COMPLETE = 0xC7
} SASISendCmd_t;

typedef enum SASIReceiveCmd {
     CAPTURE_IMAGE       = 0xCB,
     SEND_TO_COMMS       = 0x9E,
} SASIReceiveCmd_t;

uint8_t ReceiveCmd();
uint8_t SendCmd(SASISendCmd_t cmd);
uint8_t checkMailbox();

#endif