#ifndef MX25L256_H_
#define MX25L256_H_

#include <spi.h>
#include <types.h>
#include <gpio.h>

#define MX25L256_SLAVE_ID 0

#define SECTOR_SIZE 4096
#define PAGE_SIZE 256
#define ADDR_SIZE 4
#define CMD_SIZE 1

#define FILE_HEADER_PAGE 0
#define FILE_HEADER_OFFSET 1
#define IMAGE_SIZE 16384

#define PAGE_BASE(page) page * PAGE_SIZE
#define SECTOR_BASE(sector) sector * SECTOR_SIZE
#define PAGE(address) address / PAGE_SIZE
#define SECTOR(address) address / SECTOR_SIZE

typedef enum MX25L256Cmd {
    READ = 0x03,
    FASTREAD = 0x0B,
    _2READ = 0xBB,
    DREAD = 0x3B,
    QREAD = 0x6B,
    PAGE_PROGRAM = 0x02, 
    PP4B = 0x12,
    SECTOR_ERASE = 0x20,
    SECTOR_ERASE_4B = 0x21,
    BLOCK_ERASE_32K = 0x52,
    BLOCK_ERASE_64K = 0xD8,
    CHIP_ERASE = 0x60,
    WRITE_ENABLE = 0x06,
    WRITE_DISABLE = 0x04,
    READ_STATUS_REG = 0x05,
    READ_CONFIG_REG = 0x15,
    WRITE_CONFIG_REG = 0x01,
    READ_EXT_ADDR_REG = 0xC8,
    WRITE_EXT_ADDR_REG = 0xC5,
    WRITE_PROTECT_SEL = 0x68,
    ENABLE_QPI = 0x35,
    RESET_QPI = 0xF5,
    ENTER_4BYTE = 0xB7,
    EXIT_4BYTE = 0xE9,
    SUSPEND_PGRM_ERASE = 0xB0,
    RESUME_PGRM_ERASE = 0x30,
    DEEP_PWR_DOWN = 0xB9,
    RELEASE_DEEP_PWR_DOWN = 0xAB,
    SET_BURST_LENGTH = 0xC0,
    READ_FAST_BOOT_REG = 0x16,
    WRITE_FAST_BOOT_REG = 0x17,
    ERASE_FAST_BOOT_REG = 0x18,
    READ_ID = 0x9F,
    READ_ELECTRONIC_ID = 0xAB,
    ENTER_SECURED_OTP = 0xB1,
    EXIT_SECURED_OTP = 0xC1,
    READ_SECURITY_REG = 0x2B,
    WRITE_SECURITY_REG = 0x2F,
    GANG_BLOCK_LOCK = 0x7E,
    GANG_BLOCK_UNLOCK = 0x98,
    WRITE_LOCK_REG = 0x2C,
    READ_LOCK_REG = 0x2D,
    WRITE_PASSWORD_REG = 0x28,
    READ_PASSWORD_REG = 0x27,
    PASSWORD_UNLOCK = 0x29,
    WRSPB = 0xE3,
    ESSPB = 0xE4,
    READ_SPB_STATUS = 0xE2,
    SPB_LOCK_SET = 0xA6,
    SPB_LOCK_READ_REG = 0xA7,
    DPB_WRITE_REG = 0xE1,
    SPB_READ_REG = 0xE0,
    NOP = 0x00,
    RESET_ENABLE = 0x66,
    RESET_MEMORY = 0x99,
    READ4B = 0x13
} MX25L256Cmd_t;


/**
  * @brief  Initialize the MX25L256
  * @param  None
  * @retval None
  */
void MX25L256Init();

/**
  * @brief  Release deep power mode of MX25L256
  * @param  None
  * @retval None
  */
void MX25L256ReleaseDeepPWR();

/**
  * @brief  Reads the ID of the chip
  * @param  None
  * @retval Device ID
  */
uint32_t MX25L256ReadID();

/**
  * @brief  Reads the status reg of the MX25L256
  * @param  None
  * @retval Status register contents
  */
uint8_t MX25L256ReadStatusReg();

/**
  * @brief  Reads the config reg of the MX25L256
  * @param  None
  * @retval Config register contents
  */
uint8_t MX25L256ReadConfigReg();

/**
  * @brief  Writes a sector of the MX25L256
  * @param  sector: Sector to write
  * @param  tx: Pointer to the write data buffer (size 4K)
  * @retval 1 if write successful, 0 otherwise
  */
uint8_t MX25L256WriteSector(uint32_t sector, uint8_t *tx);

/**
  * @brief  Writes a page of the MX25L256
  * @param  page: Page to write 
  * @param  tx: Pointer to the write data buffer (size 256)
  * @retval 1 if write successful, 0 otherwise
  */
uint8_t MX25L256WritePage(uint32_t page, uint8_t *tx);

/**
  * @brief  Reads an address of the MX25L256
  * @param  address: Address to read 
  * @retval Address contents
  */
uint8_t MX25L256ReadAddress(uint32_t address);

/**
  * @brief  Reads a page of the MX25L256
  * @param  page: Page to read 
  * @param  rx: Pointer recieve buffer for the data (size 256)
  * @retval none
  */
void MX25L256ReadPage(uint32_t page_base, uint8_t *rx);

/**
  * @brief  Erase a sector of the MX25L256
  * @param  sector: Sector to erase
  * @retval 1 if erase successful, 0 otherwise
  */
uint8_t MX25L256EraseSector(uint32_t sector);

/**
  * @brief  Loads the FAT header 
  * @param  none
  * @retval none
  */
void FlashLoadFileHeader();

/**
  * @brief  Saves an image to flash from the IMAGE_DATA_MEM 
  * @param  none
  * @retval none
  */
void FlashSaveImage();

/**
  * @brief  Reads an image from flash and saves it in
  *         in the IMAGE_DATA_MEM 
  * @param  image: Image number to read
  * @retval none
  */
void FlashReadImage(uint8_t image);

/**
  * @brief  Reads the last image from flash and saves it in
  *         in the IMAGE_DATA_MEM 
  * @param  none
  * @retval none
  */
void FlashReadLastImage();

/**
  * @brief  Erases last saved record 
  * @param  none
  * @retval none
  */
void EraseLastRecord();

/**
  * @brief  Erases all saved records
  * @param  none
  * @retval none
  */
void FlashEraseAll();


#endif