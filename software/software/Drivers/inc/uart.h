#ifndef UART_H_
#define UART_H_

#include <system.h>
#include <types.h>
#include <altera_avalon_jtag_uart.h>
#include <altera_avalon_uart_regs.h>

void uartInit();
void sendByte(uint8_t data);
uint8_t getChar();
void getString(char *string);
uint8_t recieveCommand(char *cmd);
uint8_t strcompare(char *str1, char* str2);
void printInt(int input);

#endif