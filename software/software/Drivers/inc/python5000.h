#ifndef PYTHON5000_H_
#define PYTHON5000_H_

#include <system.h>
#include <types.h>
#include <uart.h>

#define PYTHON5000_SLAVE_ID        0

// uncomment the desired mode
#define MUX1
// #define MUX2
// #define MUX4
// #define MUX8

#define PYTHON_5000_WIDTH          2592
#define PYTHON_5000_HEIGHT         2048

#define CAPTURE_TIMEOUT            5000

#define ROI                        0
#define X_ROI                      288
#define Y_ROI                      226

#define X_START                    80//50
#define X_END                      100//70
#define Y_START                    1300//1000 lower
#define Y_END                      1520//1220

#define PLL_LOCK_INDICATION_MASK   0x01
#define SUBSAMPLING_MASK           0x80
#define SEQUENCER_ENABLE_MASK      0x01  
#define TRIGGER_ENABLE_MASK        0x10
#define AEC_ENABLE_MASK            0x01
#define AEC_LOCKED                 0x800

#define ROI_REG_OFFSET             0x03

#define RX_PLL_LOCK_MASK           0x01
#define READING_IMAGE              0x02
#define IMAGE_DATA				         0x04
#define IMAGE_LINE_LENGTH_MASK     0xFFF0
#define IMAGE_COUNTER_ADDRESS_MASK 0x3FFF0000

static uint8_t *image_mem_base = (uint8_t *)IMAGE_DATA_MEM_BASE;
static uint8_t *image_mem_end = (uint8_t *)(IMAGE_DATA_MEM_BASE + IMAGE_DATA_MEM_SPAN - 1);

static uint32_t *pythonStatusPtr = (uint32_t *)PYTHON_STATUS_BASE;

typedef enum PYTHON5000reg {
  REG_CHIP_ID		      = 0,
  REG_CHIP_INFO		    = 1,
  REG_CHIP_CONFIG	    = 2,
  SOFT_RESET_PLL		  = 8,
  SOFT_RESET_CGEN	    = 9,
  SOFT_RESET_ANALOG	  = 10,
  PLL_CONFIG		      = 16,
  IO_CONFIG			      = 20,
  PLL_LOCK			      = 24,
  CLOCK_CONFIG		    = 32,
  CLOCK_ENABLE		    = 34,
  IMAGE_CORE_CONFIG0  = 40,
  IMAGE_CORE_CONFIG1  = 41,
  AFE_CONFIG		      = 48,
  BIAS_PWR_CONFIG	    = 64,
  BIAS_CONFIG	    	  = 65,
  AFE_BIAS_CONFIG     = 66,
  COLUMN_BIAS_CONFIG	= 67,
  LVDS_BIAS_CONFIG 	  = 68,
  ADC_BIAS		      	= 69,
  CHARGE_PUMP_CONFIG	= 72,
  TEMP_SENSOR_CONFIG	= 96,
  TEMP_SENSOR_STATUS	= 97,
  LVDS_CONFIG	    	  = 112,
  TRAINING_PATTERN    = 116,
  BLACK_CAL			      = 128,
  GENERAL_CONFIG      = 129,
  TEST_PAT_CONFIG  	  = 144,
  TEST_PAT_0_1		    = 146,
  TEST_PAT_2_3		    = 147,
  TEST_PAT_MSB_0123  	= 150,
  AEC_CONFIG          = 160,
  AEC_INTENSITY		    = 161,
  MIN_EXPOSURE        = 168,
  MIN_GAIN            = 169,
  MAX_EXPOSURE        = 170,
  MAX_GAIN            = 171,
  AEC_STATUS          = 186,
  SEQ_GEN_CONFIG	    = 192,
  SEQ_DEL_CONFIG	  	= 193,
  INTEGRATION_CONTROL = 194,
  ACTIVE_ROI		      = 195,
  BLACK_LINE_CONFIG	  = 197,
  MULT_TIMER_0		    = 199,
  FR_LENGTH0		      = 200,
  EXPOSURE0		      	= 201,
  ANALOG_GAIN	      	= 204,
  DIGITAL_GAIN	      = 205,
  REF_LINES           = 207,
  ACTIVE_ROI1         = 228,
  AEC_ROI_X           = 253,
  AEC_ROI_Y_START     = 254,
  AEC_ROI_Y_END       = 255,
  ROI0_X_CONFIG	    	= 256,
  ROI0_Y_START	      = 257,
  ROI0_Y_END	        = 258,
  ROI1_X_CONFIG	     	= 259,
  ROI1_Y_START	      = 260,
  ROI1_Y_END	        = 261,
  ROI2_X_CONFIG	    	= 262,
  ROI2_Y_START	      = 263,
  ROI2_Y_END	        = 264,
  ROI3_X_CONFIG	    	= 265,
  ROI3_Y_START	      = 266,
  ROI3_Y_END	        = 267,
  ROI4_X_CONFIG	    	= 268,
  ROI4_Y_START	      = 269,
  ROI4_Y_END	        = 270,
  ROI5_X_CONFIG	    	= 271,
  ROI5_Y_START		    = 272,
  ROI5_Y_END		      = 273,
  ROI6_X_CONFIG	    	= 274,
  ROI6_Y_START	      = 275,
  ROI6_Y_END		      = 276,
  ROI7_X_CONFIG    		= 277,
  ROI7_Y_START	      = 278,
  ROI7_Y_END	        = 279
} PYTHON5000reg_t;

/*****Public Functions*****/

/**
  * @brief  Gets the value on the sync channel
  * @param  None
  * @retval Sync value
  */
uint16_t PYTHON5000GetLineLength();

/**
  * @brief  Gets the size of the image 
  * @param  None
  * @retval Image size
  */
uint16_t PYTHON5000GetImageSize();

/**
  * @brief  Calls all required python5000 init functions
  * @param  Number of channels
  * @retval None
  */
void PYTHON5000Init();

/**
  * @brief  Initializes all clock registers
  * @param  None
  * @retval None
  */
void PYTHON5000EnableClockMngmnt();

/**
  * @brief  Exexutes required register uploads to get an
  *         image out. (apparently)
  * @param  None
  * @retval None
  */
void PYTHON5000InitializeCoreRegisters();

/**
  * @brief  Powers up all required blocks
  * @param  None
  * @retval None
  */
void PYTHON5000SoftPowerUp();

/**
  * @brief  Enables PYTHON5000 sequencer
  * @param  None
  * @retval None
  */
void PYTHON5000EnableSquencer();

/**
  * @brief  Disables PYTHON5000 sequencer
  * @param  None
  * @retval None
  */
void PYTHON5000DisableSequencer();

/**
  * @brief  Enables PYTHON5000 subsampling
  * @param  None
  * @retval None
  */
void PYTHON5000EnableSubsampling();

/**
  * @brief  Disables PYTHON5000 subsampling
  * @param  None
  * @retval None
  */
void PYTHON5000DisableSubsampling();

/**
  * @brief  Enables selected PYTHON5000 ROI
  * @param  None
  * @retval None
  */
void PYTHON5000EnableROI(uint16_t roi);

/**
  * @brief  Disables all PYTHON5000 ROIs
  * @param  None
  * @retval None
  */
void PYTHON5000DisableROI();

/**
  * @brief  Enables PYTHON5000 Trigger mode
  * @param  None
  * @retval None
  */
void PYTHON5000EnableTrigger();

/**
  * @brief  Disables PYTHON5000 Trigger mode
  * @param  None
  * @retval None
  */
void PYTHON5000DisableTrigger();

/**
  * @brief  Puts test patterns on data lines
  * @param  none
  * @retval none
  */
void PYTHON5000EnableTestPattern();

/**
  * @brief  Disables test patterns on data lines
  * @param  none
  * @retval none
  */
void PYTHON5000DisableTestPattern();

/**
  * @brief  Enables PYTHON5000 automatic exposure control (AEC)
  * @param  none
  * @retval none
  */
void PYTHON5000EnableAEC();

/**
  * @brief  Disables PYTHON5000 automatic exposure control (AEC)
  * @param  none
  * @retval none
  */
void PYTHON5000DisableAEC();

/**
  * @brief  Sets PYTHON5000 region of interest for exposure control
  * 		  - should the the same as the active ROI
  * @param  none
  * @retval none
  */
void PYTHON5000SetAECROI(uint16_t xstart, uint16_t ystart, uint16_t xend, uint16_t yend);

/**
  * @brief  Sets PYTHON5000 region of interest (max 16K pixels)
  * @param  none
  * @retval none
  */
void PYTHON5000SetROI(uint8_t roi, uint16_t xstart, uint16_t ystart, uint16_t xend, uint16_t yend);

/**
  * @brief  Aligns LVDS data
  * @param  none
  * @retval none
  */
void PYTHON5000AlignData();

/**
  * @brief  Triggers PYTHON to take photo
  * @param  none
  * @retval none
  */
uint8_t PYTHON5000Capture();

/**
  * @brief  Automatically sets exposure
  * @param  none
  * @retval none
  */
void PYTHON5000SetExposure();

/**
  * @brief  Writes image to UART
  * @param  none
  * @retval none
  */
void displayImage();

/**
  * @brief  Gets the temperature of the python
  * @param  none
  * @retval temperature value
  */
uint32_t PYTHON5000GetTemp();



#endif
