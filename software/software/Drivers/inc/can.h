#ifndef CAN_H_
#define CAN_H_

#include <system.h>
#include <types.h>
#include <command.h>

#define CAN_TX_ENABLE 0x01
#define CAN_RESET 0x02
#define CAN_TX_DONE 0x04
#define CAN_RX_FULL 0x08

#define CAN_RX_REG_MASK 0xFF00
#define CAN_TX_REG_MASK 0xFF0000

#define CAN_TX_TIMEOUT 10

void CANInit();
void CANTransmit(uint8_t data);
uint8_t CANReceive();
uint8_t CANReadStatus();

#endif
