#ifndef GPIO_H_
#define GPIO_H_

#include <system.h>
#include <types.h>
#include <unistd.h>

#define SETBIT(bit, value, data) data ^ ((-value ^ data) & (0x01 << bit))
#define GPIO_LOW 0
#define GPIO_HIGH 1

typedef enum GPOutput
{
  TRIGGER0,
  TRIGGER1,
  TRIGGER2,
  BITSLIP,
  ADDR_CNT_ENABLE,
  ADDR_CNT_RESET,
  IMAGE_MEM_CS,
  IMAGE_MEM_CLKEN,
  IMAGE_MEM_WREN,
  PYTHON_RST,
  FLASH_RST,
  FLASH_WP,
  LVDS_PLL_RESET,
  GPOUTPUT_SIZE
} GPOutput_t;


/**
  * @brief  Writes a single bit value to GPOutput
  * @param  output: Pin name
  * @param  value: binary value
  * @retval None
  */
void GPIOWriteBit(GPOutput_t output, int8_t value);

/**
  * @brief  Enable external write to memory
  * @param  none
  * @retval None
  */
void ImageMemWriteEnable();

/**
  * @brief  Disables external write to memory
  * @param  none
  * @retval None
  */
void ImageMemWriteDisable();

/**
  * @brief  Resets the image mem address counter
  * @param  none
  * @retval None
  */
void ResetAddressCounter();

/**
  * @brief  Enables the image mem address counter
  * @param  none
  * @retval None
  */
void EnableAddressCounter();

#endif