#include <can.h>
#include <command.h>

void CANInit() {
    volatile uint32_t *can_base = (uint32_t *)CAN_REG_BASE;
    *can_base = 0x02;
    *can_base = 0x00;
}

// Write to the Tx reg
void CANTransmit(uint8_t data) {
    // figure out how to YEET into the tx reg
    volatile uint32_t *can_base = (uint32_t *)CAN_REG_BASE;
    volatile uint32_t contents = *can_base;
    volatile uint32_t ticks = 0;

    *can_base = contents | 0x02;
    *can_base = contents & ~(uint32_t)0x02;

    contents = ((data << 16) & CAN_TX_REG_MASK) | (contents & 0xFFFF) | CAN_TX_ENABLE;

    *can_base = contents;
    while(!(*can_base & CAN_TX_DONE)) {
    	ticks++;
        if(ticks > CAN_TX_TIMEOUT)
            *can_base = contents;
        	ticks = 0;
    }
    contents &= ~((uint32_t)(CAN_TX_DONE | CAN_TX_ENABLE));
    *can_base = contents;
}

// Read from the Rx reg
uint8_t CANReceive() {
    //figure out how to pull from the Rx reg
    volatile uint32_t *can_base = (uint32_t *)CAN_REG_BASE;
    volatile uint32_t contents;
    volatile uint8_t rx_data;

    while(!(*can_base & CAN_RX_FULL));
    rx_data = (*can_base & CAN_RX_REG_MASK) >> 8; // this is not going to work unfortunately
    contents = *can_base;
    contents &= ~(uint32_t)CAN_RX_FULL;
    *can_base = contents;
    return rx_data;
}

// read the status reg
uint8_t CANReadStatus() {
    // get the status register data
    volatile uint32_t *can_base = (uint32_t *)CAN_REG_BASE;
    volatile uint32_t contents = *can_base;

    uint8_t status = contents & 0x0F;
    return status;
}
