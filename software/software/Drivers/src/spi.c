#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include <system.h>
#include <types.h>
#include <spi.h>

void SPI0TransferBlocking(alt_u32 slave,
                         const alt_u32 *write_data,
                         alt_u32 *read_data,
                         alt_u32 size,
                         alt_u32 flags)
{
  alt_u32 write_length = size;
  alt_u32 base = SPI_0_BASE;
  alt_u32 *rxptr = (alt_u32 *)(base + ALTERA_AVALON_SPI_RXDATA_REG);
  const alt_u32 *write_end = write_data + write_length;

  alt_u32 status;

  /* We must not send more than two bytes to the target before it has
   * returned any as otherwise it will overflow. */
  /* Unfortunately the hardware does not seem to work with credits > 1,
   * leave it at 1 for now. */
  alt_32 credits = 1;

  /* Warning: this function is not currently safe if called in a multi-threaded
   * environment, something above must perform locking to make it safe if more
   * than one thread intends to use it.
   */

  IOWR_ALTERA_AVALON_SPI_SLAVE_SEL(base, 1 << slave);

  /* Set the SSO bit (force chipselect) only if the toggle flag is not set */
  if ((flags & ALT_AVALON_SPI_COMMAND_TOGGLE_SS_N) == 0)
  {
    IOWR_ALTERA_AVALON_SPI_CONTROL(base, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);
  }

  /*
   * Discard any stale data present in the RXDATA register, in case
   * previous communication was interrupted and stale data was left
   * behind.
   */
  IORD_ALTERA_AVALON_SPI_RXDATA(base);

  /* Keep clocking until all the data has been processed. */
  while (write_data < write_end)
  {

    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(base);
    } while ((status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK) == 0 || credits == 0);

    
    credits--;
    IOWR_ALTERA_AVALON_SPI_TXDATA(base, *write_data++);

    if (write_data == write_end)
      IOWR_ALTERA_AVALON_SPI_CONTROL(base, 0);
      
    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(base);
    } while ((status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK) == 0);
    *read_data++ = IORD_ALTERA_AVALON_SPI_RXDATA(base);
    credits++;

  }

  /* Wait until the interface has finished transmitting */
  do
  {
    status = IORD_ALTERA_AVALON_SPI_STATUS(base);
  } while ((status & ALTERA_AVALON_SPI_STATUS_TMT_MSK) == 0);
}

void SPI1TransferBlocking(alt_u32 slave,
                        const alt_u8 *write_data,
                        alt_u8 *read_data,
                        alt_u32 size,
                        alt_u32 flags)
{
  alt_u32 base = SPI_1_BASE;
  const alt_u8 *write_end = write_data + size;

  alt_u32 status;

  /* We must not send more than two bytes to the target before it has
   * returned any as otherwise it will overflow. */
  /* Unfortunately the hardware does not seem to work with credits > 1,
   * leave it at 1 for now. */
  alt_32 credits = 1;

  /* Warning: this function is not currently safe if called in a multi-threaded
   * environment, something above must perform locking to make it safe if more
   * than one thread intends to use it.
   */

  IOWR_ALTERA_AVALON_SPI_SLAVE_SEL(base, 1 << slave);

  /* Set the SSO bit (force chipselect) only if the toggle flag is not set */
  if ((flags & ALT_AVALON_SPI_COMMAND_TOGGLE_SS_N) == 0)
  {
    IOWR_ALTERA_AVALON_SPI_CONTROL(base, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);
  }

  /*
   * Discard any stale data present in the RXDATA register, in case
   * previous communication was interrupted and stale data was left
   * behind.
   */
  IORD_ALTERA_AVALON_SPI_RXDATA(base);

  /* Keep clocking until all the data has been processed. */
  while (write_data < write_end)
  {

    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(base);
    } while ((status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK) == 0 || credits == 0);

    
    credits--;
    IOWR_ALTERA_AVALON_SPI_TXDATA(base, *write_data++);

      
    do
    {
      status = IORD_ALTERA_AVALON_SPI_STATUS(base);
    } while ((status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK) == 0);
    *read_data++ = IORD_ALTERA_AVALON_SPI_RXDATA(base);
    credits++;

    if (write_data == write_end)
    	IOWR_ALTERA_AVALON_SPI_CONTROL(base, 0);
  }

  /* Wait until the interface has finished transmitting */
  do
  {
    status = IORD_ALTERA_AVALON_SPI_STATUS(base);
  } while ((status & ALTERA_AVALON_SPI_STATUS_TMT_MSK) == 0);
}
