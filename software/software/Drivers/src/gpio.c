#include <gpio.h>
#include <altera_avalon_pio_regs.h>

/*****Public Functions*****/


/**
  * @brief  Writes a single bit value to GPOutput
  * @param  output: Pin name
  * @param  value: binary value
  * @retval None
  */
void GPIOWriteBit(GPOutput_t output, int8_t value) {
     uint32_t databuf;

     value &= 0x01;
     databuf = IORD_ALTERA_AVALON_PIO_DATA(GPOUTPUT_BASE) & ((0x01 << GPOUTPUT_SIZE) - 1);
     databuf ^= ((-value ^ databuf) & (0x01 << output));
     IOWR_ALTERA_AVALON_PIO_DATA(GPOUTPUT_BASE, databuf);
}

/**
  * @brief  Enable external write to memory
  * @param  none
  * @retval None
  */
void ImageMemWriteEnable() {
	GPIOWriteBit(IMAGE_MEM_CS, GPIO_HIGH);
	GPIOWriteBit(IMAGE_MEM_WREN, GPIO_HIGH);
	GPIOWriteBit(IMAGE_MEM_CLKEN, GPIO_HIGH);
}

/**
  * @brief  Disables external write to memory
  * @param  none
  * @retval None
  */
void ImageMemWriteDisable() {
	GPIOWriteBit(IMAGE_MEM_CS, GPIO_LOW);
	GPIOWriteBit(IMAGE_MEM_WREN, GPIO_LOW);
	GPIOWriteBit(IMAGE_MEM_CLKEN, GPIO_LOW);
}


/**
  * @brief  Resets the image mem address counter
  * @param  none
  * @retval None
  */
void ResetAddressCounter() {
  GPIOWriteBit(ADDR_CNT_ENABLE, GPIO_LOW);
  GPIOWriteBit(ADDR_CNT_RESET, GPIO_HIGH);
  GPIOWriteBit(ADDR_CNT_RESET, GPIO_LOW);
}

/**
  * @brief  Enables the image mem address counter
  * @param  none
  * @retval None
  */
void EnableAddressCounter() {
  ResetAddressCounter();
	GPIOWriteBit(ADDR_CNT_ENABLE, GPIO_HIGH);
}


