#include "MX25L256.h"

static uint8_t FlashFileHeader[PAGE_SIZE];
static uint8_t FileHeaderIndex;

/*****Private Functions*****/

void MX25L256SendCommand(MX25L256Cmd_t cmd, uint8_t *tx, uint32_t txsize, uint8_t *rx, uint32_t rxsize) {

    tx[0] = cmd;
    SPI1TransferBlocking(MX25L256_SLAVE_ID, tx, rx, (txsize + CMD_SIZE) + rxsize, 0);
}

void MX25L256WriteEnable() {
    uint8_t tx[10];
    uint8_t rx[10];

    while(!(MX25L256ReadStatusReg() & 0x02))
        MX25L256SendCommand(WRITE_ENABLE, tx, 0, rx, 0);
}

void MX25L256WriteDisable() {
    uint8_t tx[10];
    uint8_t rx[10];

    while(MX25L256ReadStatusReg() & 0x02)
        MX25L256SendCommand(WRITE_DISABLE, tx, 0, rx, 0);
}

uint8_t MX25L256ReadSecurityReg() {
    uint8_t tx[10];
    uint8_t rx[10];

    MX25L256SendCommand(READ_SECURITY_REG, tx, 0, rx, 1);

    return rx[1];
}

/*****Public functions*****/


/**
  * @brief  Initialize the MX25L256
  * @param  None
  * @retval None
  */
void MX25L256Init() {
    GPIOWriteBit(FLASH_RST, GPIO_HIGH);
    GPIOWriteBit(FLASH_WP, GPIO_HIGH);

    uint8_t tx[10];
    uint8_t rx[10];

    MX25L256WriteEnable();
    while(!(MX25L256ReadConfigReg() == 0x20))
        MX25L256SendCommand(ENTER_4BYTE, tx, 0, rx, 0);
    MX25L256WriteDisable();
}


/**
  * @brief  Release deep power mode of MX25L256
  * @param  None
  * @retval None
  */
void MX25L256ReleaseDeepPWR() {
    uint8_t rx[10];
    uint8_t tx[10];
    MX25L256SendCommand(RELEASE_DEEP_PWR_DOWN, tx, 0, rx, 0);
}


/**
  * @brief  Reads the ID of the chip
  * @param  None
  * @retval Device ID
  */
uint32_t MX25L256ReadID() {
    uint8_t rx[10];
    uint8_t tx[10];
    MX25L256SendCommand(READ_ID, tx, 0, rx, 3);

    return (uint32_t)rx[1] << 16 | (uint32_t)rx[2] << 8 | rx[3];
}


/**
  * @brief  Reads the status reg of the MX25L256
  * @param  None
  * @retval Status register contents
  */
uint8_t MX25L256ReadStatusReg() {
    uint8_t rx[10];
    uint8_t tx[10];
    MX25L256SendCommand(READ_STATUS_REG, tx, 0, rx, 1);

    return rx[1];
}


/**
  * @brief  Reads the config reg of the MX25L256
  * @param  None
  * @retval Config register contents
  */
uint8_t MX25L256ReadConfigReg() {
    uint8_t rx[10];
    uint8_t tx[10];
    MX25L256SendCommand(READ_CONFIG_REG, tx, 0, rx, 1);

    return rx[1];
}


/**
  * @brief  Writes a sector of the MX25L256
  * @param  sector: Sector to write
  * @param  tx: Pointer to the write data buffer (size 4K)
  * @retval 1 if write successful, 0 otherwise
  */
uint8_t MX25L256WriteSector(uint32_t sector, uint8_t *tx) {
    uint8_t *txptrbuf = tx;

    uint32_t sector_base = SECTOR_BASE(sector);
    uint32_t page = sector_base / PAGE_SIZE;

    for(int i = 0; i < SECTOR_SIZE / PAGE_SIZE; i++) {
        MX25L256WritePage(page++, txptrbuf);
        txptrbuf += PAGE_SIZE;
    }

    return 1;
}


/**
  * @brief  Writes a page of the MX25L256
  * @param  page: Page to write 
  * @param  tx: Pointer to the write data buffer (size 256)
  * @retval 1 if write successful, 0 otherwise
  */
uint8_t MX25L256WritePage(uint32_t page, uint8_t *tx) {
    uint8_t rx[PAGE_SIZE + ADDR_SIZE + CMD_SIZE];
    uint8_t txbuf[PAGE_SIZE + ADDR_SIZE + CMD_SIZE] = {NOP};

    for(int i = 0; i < PAGE_SIZE; i++) {
        txbuf[i + ADDR_SIZE + CMD_SIZE] = tx[i];
    }

    uint32_t page_base = PAGE_BASE(page);

    txbuf[4] = page_base & 0xFF;
    page_base >>= 8;
    txbuf[3] = page_base & 0xFF;
    page_base >>= 8;
    txbuf[2] = page_base & 0xFF;
    page_base >>= 8;
    txbuf[1] = page_base & 0xFF;
    page_base >>= 8;

    MX25L256WriteEnable();
    MX25L256SendCommand(PP4B, txbuf, PAGE_SIZE + ADDR_SIZE, rx, 0);
    MX25L256WriteDisable();    

    if(MX25L256ReadSecurityReg() & 0x20)  //check if write was successful
        return 0;
    else
        return 1;
}

/**
  * @brief  Reads an address of the MX25L256
  * @param  address: Address to read 
  * @retval Address contents
  */
uint8_t MX25L256ReadAddress(uint32_t address) {
    uint8_t tx[10];
    uint8_t rx[10];

    tx[4] = address & 0xFF;
    address >>= 8;
    tx[3] = address & 0xFF;
    address >>= 8;
    tx[2] = address & 0xFF;
    address >>= 8;
    tx[1] = address & 0xFF;
    address >>= 8;
    MX25L256SendCommand(READ4B, tx, 4, rx, 1);

    return rx[5];
}


/**
  * @brief  Reads a sector of the MX25L256
  * @param  page: Page to read 
  * @param  rx: Pointer recieve buffer for the data (size 4K)
  * @retval none
  */
void MX25L256ReadSector(uint32_t sector, uint8_t *rx) {
    uint8_t *rxptrbuf = rx;

    uint32_t sector_base = SECTOR_BASE(sector);
    uint32_t page = sector_base / PAGE_SIZE;

    for(int i = 0; i < SECTOR_SIZE / PAGE_SIZE; i++) {
        MX25L256ReadPage(page++, rxptrbuf);
        rxptrbuf += PAGE_SIZE;
    }
}

/**
  * @brief  Reads a page of the MX25L256
  * @param  page: Page to read 
  * @param  rx: Pointer recieve buffer for the data (size 256)
  * @retval none
  */
void MX25L256ReadPage(uint32_t page, uint8_t *rx) {
    uint8_t tx[PAGE_SIZE + CMD_SIZE + ADDR_SIZE] = {NOP};
    uint8_t rxbuf[PAGE_SIZE + CMD_SIZE + ADDR_SIZE];

    uint32_t page_base = PAGE_BASE(page);

    tx[4] = page_base & 0xFF;
    page_base >>= 8;
    tx[3] = page_base & 0xFF;
    page_base >>= 8;
    tx[2] = page_base & 0xFF;
    page_base >>= 8;
    tx[1] = page_base & 0xFF;
    page_base >>= 8;

    MX25L256SendCommand(READ4B, tx, 4, rxbuf, PAGE_SIZE);
    for(int i = 0; i < PAGE_SIZE; i++) {
        rx[i] = rxbuf[i + CMD_SIZE + ADDR_SIZE];
    }
}


/**
  * @brief  Erase a sector of the MX25L256
  * @param  sector: Sector to erase
  * @retval 1 if erase successful, 0 otherwise
  */
uint8_t MX25L256EraseSector(uint32_t sector) {
    uint8_t tx[10];
    uint8_t rx[10];

    uint32_t sector_base = SECTOR_BASE(sector);

    tx[4] = sector_base & 0xFF;
    sector_base >>= 8;
    tx[3] = sector_base & 0xFF;
    sector_base >>= 8;
    tx[2] = sector_base & 0xFF;
    sector_base >>= 8;
    tx[1] = sector_base & 0xFF;
    sector_base >>= 8;

    MX25L256WriteEnable();
    MX25L256SendCommand(SECTOR_ERASE_4B, tx, 4, rx, 0);    
    MX25L256WriteDisable();

    if(MX25L256ReadSecurityReg() & 0x40) //check if erase was successful
        return 0;
    else 
        return 1;
}


/**
  * @brief  Loads the FAT header 
  * @param  none
  * @retval none
  */
void FlashLoadFileHeader() {
    MX25L256ReadPage(FILE_HEADER_PAGE, FlashFileHeader);
    while(FlashFileHeader[FileHeaderIndex]) {
        FileHeaderIndex++;
    }
}


/**
  * @brief  Saves an image to flash from the IMAGE_DATA_MEM 
  * @param  none
  * @retval none
  */
void FlashSaveImage() {
    uint8_t *image_data_ptr = (uint8_t *)IMAGE_DATA_MEM_BASE;
    uint32_t image_base_sector = FileHeaderIndex * IMAGE_SIZE / SECTOR_SIZE + FILE_HEADER_OFFSET;

    for(int sector = image_base_sector; sector - image_base_sector < IMAGE_SIZE / SECTOR_SIZE; sector++) {
        MX25L256EraseSector(sector);
        MX25L256WriteSector(sector, image_data_ptr);
        image_data_ptr += SECTOR_SIZE;
    }

    MX25L256EraseSector(FILE_HEADER_PAGE);
    FlashFileHeader[FileHeaderIndex++] = 1;
    MX25L256WritePage(FILE_HEADER_PAGE, FlashFileHeader);
}


/**
  * @brief  Reads an image from flash and saves it in
  *         in the IMAGE_DATA_MEM 
  * @param  image: Image number to read
  * @retval none
  */
void FlashReadImage(uint8_t image) {
    uint8_t *image_data_ptr = (uint8_t *)IMAGE_DATA_MEM_BASE;
    uint32_t image_base_sector = image * IMAGE_SIZE / SECTOR_SIZE + FILE_HEADER_OFFSET;

    if(FlashFileHeader[image]) {
        for(int sector = image_base_sector; sector - image_base_sector < IMAGE_SIZE / SECTOR_SIZE; sector++) {
            MX25L256ReadSector(sector, image_data_ptr);
            image_data_ptr += SECTOR_SIZE;
        }   
    }

}

/**
  * @brief  Reads the last image from flash and saves it in
  *         in the IMAGE_DATA_MEM 
  * @param  none
  * @retval none
  */
void FlashReadLastImage() {
    uint8_t *image_data_ptr = (uint8_t *)IMAGE_DATA_MEM_BASE;
    uint32_t image_base_sector = (FileHeaderIndex - 1) * IMAGE_SIZE / SECTOR_SIZE + FILE_HEADER_OFFSET;

    for(int sector = image_base_sector; sector - image_base_sector < IMAGE_SIZE / SECTOR_SIZE; sector++) {
        MX25L256ReadSector(sector, image_data_ptr);
        image_data_ptr += SECTOR_SIZE;
    }   

}



/**
  * @brief  Erases last saved record 
  * @param  none
  * @retval none
  */
void EraseLastRecord() {

    if(FileHeaderIndex > 0) {        
        uint32_t image_base_sector = --FileHeaderIndex * IMAGE_SIZE / SECTOR_SIZE + FILE_HEADER_OFFSET;

        for(int sector = image_base_sector; sector - image_base_sector < IMAGE_SIZE / SECTOR_SIZE; sector++) {
            MX25L256EraseSector(sector);
        }

        MX25L256EraseSector(FILE_HEADER_PAGE);
        FlashFileHeader[FileHeaderIndex] = 0;
        MX25L256WritePage(FILE_HEADER_PAGE, FlashFileHeader);
    }
}


/**
  * @brief  Erases all saved records
  * @param  none
  * @retval none
  */
void FlashEraseAll() {
    uint8_t tx[256] = {0};

    MX25L256WritePage(FILE_HEADER_PAGE, tx);
    FlashLoadFileHeader();
}
