#include <uart.h>

void uartInit() {
     IOWR_ALTERA_AVALON_UART_CONTROL(UART_BASE, 0x0000);
}

void sendByte(uint8_t data) {
     while(!(IORD_ALTERA_AVALON_UART_STATUS(UART_BASE) & ALTERA_AVALON_UART_CONTROL_TMT_MSK));
     while(!(IORD_ALTERA_AVALON_UART_STATUS(UART_BASE) & ALTERA_AVALON_UART_STATUS_TRDY_MSK));

     IOWR_ALTERA_AVALON_UART_TXDATA(UART_BASE, data);
}

uint8_t getChar() {
     while(!(IORD_ALTERA_AVALON_UART_STATUS(UART_BASE) & ALTERA_AVALON_UART_STATUS_RRDY_MSK));
     return (uint8_t)IORD_ALTERA_AVALON_UART_RXDATA(UART_BASE);
}

void getString(char *string) {
     volatile char buf;
     while((buf = getChar()) != '\r') {
    	 //sendByte(buf);
          *string++ = buf;
     }
     *string = 0;
}

uint8_t recieveCommand(char *cmd) {
     char buf[20];

     getString(buf);
     return strcompare(cmd, buf);
}

uint8_t strcompare(char *str1, char* str2) {
     while(*str1 || *str2) {
          if(*str1++ != *str2++)
               return 0;
     }

     return 1;
}

void printInt(int input) {
    int result[20];
    int i = 0;

    if (!input)
        sendByte('0'); //print zero if input = 0
    else if (input > 0)
    {
        while (input)
        {
            result[i] = input % 10; //store next decimal value
            input /= 10;            //base 10 shift right
            i++;
        }
        i--;

        while (i + 1)
        {
            sendByte((char)result[i--] + '0'); //print ASCII value of integer
        }
    }
    else if (input < 0) //check for negative
    {
        sendByte('-'); //print '-' to represent negative integer
        input *= -1;  //take magnitude
        while (input)
        {
            result[i] = input % 10; //store next decimal value
            input /= 10;            //base 10 shift right
            i++;
        }
        i--;

        while (i + 1)
        {
            sendByte((char)result[i--] + '0'); //print ASCII value of integer
        }
    }
} //end printInt
