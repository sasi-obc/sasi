#include <python5000.h>
#include <spi.h>
#include <gpio.h>
#include <MX25L256.h>

/*****Global variables*****/
static uint32_t pythonStatus;
static uint8_t trigger = 0;

/*****Private Functions*****/

void PYTHON5000WriteReg(PYTHON5000reg_t reg, uint16_t data) {
	uint32_t tx;
	uint32_t rx;
	tx = ((reg << 17) & 0x3FE0000) | 0x10000;
	tx |= data;

	SPI0TransferBlocking(PYTHON5000_SLAVE_ID, &tx, &rx, 1, 0);
}

uint16_t PYTHON5000ReadReg(PYTHON5000reg_t reg) {
	uint32_t tx[2] = {0};
	uint32_t rx[2] = {0};
	uint16_t regData;

	tx[0] = (reg << 17) & 0x3FE0000;

	SPI0TransferBlocking(PYTHON5000_SLAVE_ID, tx, rx, 2, 0);
	regData = (uint16_t)((rx[0] << 1) | ((rx[1] >> 25) & 0x01)); // must do this for read to compensate for falling edge

	return regData;
}

/**
  * @brief  Triggers PYTHON shutter
  * @param  none
  * @retval None
  */
void Trigger() {
  GPIOWriteBit(TRIGGER0, GPIO_HIGH);
  usleep(10);
  GPIOWriteBit(TRIGGER0, GPIO_LOW);
}

/**
  * @brief  Reverses every odd kernel
  * @param  none
  * @retval None
  */
void arrangeImage() {
	volatile uint8_t *pixel = image_mem_base;
	volatile uint8_t *image_end = pixel + (PYTHON5000GetImageSize() - 1);
	volatile uint8_t odd = 1;
	volatile uint8_t *kernel;
	volatile uint8_t rearrangedKernel[8];

	while(pixel < image_end) {
		odd ^= 0x01;

		if(odd) {
			kernel = pixel + 7;
			for(int i = 0; i < 8; i++) {
				rearrangedKernel[i] = *kernel--; 
			}

			for(int i = 0; i < 8; i++) {
				*pixel++ = rearrangedKernel[i];
			}
		}
		else
			pixel += 8;
	}
}

uint32_t PYTHON5000GetStatus() {
	return *pythonStatusPtr;
}

/**
  * @brief  Updates pythonStatus variable
  * @param  None
  * @retval None
  */
void PYTHON5000UpdateStatus() {
	pythonStatus = *pythonStatusPtr;
}

/*****Public Functions*****/


/**
  * @brief  Gets the number of pixels in a line
  * @param  None
  * @retval Pixels per line
  */
uint16_t PYTHON5000GetLineLength() {
	return (pythonStatus & IMAGE_LINE_LENGTH_MASK) >> 4;
}

/**
  * @brief  Gets the size of the image 
  * @param  None
  * @retval Image size
  */
uint16_t PYTHON5000GetImageSize() {
	return (pythonStatus & IMAGE_COUNTER_ADDRESS_MASK) >> 16;
}


/**
  * @brief  Calls all required python5000 init functions
  * @param  Number of channels
  * @retval None
  */
void PYTHON5000Init() {
	
	GPIOWriteBit(PYTHON_RST, GPIO_LOW);
	GPIOWriteBit(TRIGGER0, GPIO_LOW);
	GPIOWriteBit(TRIGGER1, GPIO_LOW);
	GPIOWriteBit(TRIGGER2, GPIO_LOW);
	GPIOWriteBit(BITSLIP, GPIO_LOW);
	ResetAddressCounter();
	ImageMemWriteDisable();
	GPIOWriteBit(LVDS_PLL_RESET, GPIO_LOW);
	GPIOWriteBit(PYTHON_RST, GPIO_HIGH);

	PYTHON5000EnableClockMngmnt();
	PYTHON5000InitializeCoreRegisters();
	PYTHON5000SoftPowerUp();

	while(!(PYTHON5000GetStatus() & RX_PLL_LOCK_MASK));

	PYTHON5000AlignData();
	PYTHON5000EnableAEC();
	PYTHON5000SetAECROI(X_START, Y_START, X_END, Y_END);
	PYTHON5000EnableSubsampling();
	PYTHON5000DisableTrigger();
	PYTHON5000SetROI(ROI, X_START, Y_START, X_END, Y_END);
	PYTHON5000EnableROI(ROI);
	PYTHON5000EnableSquencer();
}

/**
  * @brief  Initializes all clock registers
  * @param  None
  * @retval None
  */
void PYTHON5000EnableClockMngmnt() {

	PYTHON5000WriteReg(REG_CHIP_CONFIG, 0x000D); // set bayer-colour
	PYTHON5000WriteReg(17, 0x2113);			// configure PLL
	PYTHON5000WriteReg(IO_CONFIG, 0x0000);       // power down clock input
	PYTHON5000WriteReg(26, 0x2280);			// configure pll lock detector
	PYTHON5000WriteReg(27, 0x3D2D);			// configure pll lock detector


	#ifdef MUX8
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0004);    // mux 8 data-channel and disable logic clock
	#elif defined MUX4
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0014);    // mux 4 data-channel and disable logic clock
	#elif defined MUX2
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0024);    // mux 2 data-channel and disable logic clock
	#elif defined MUX1
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0034);    // mux 1 data-channel and disable logic clock
	#endif

	PYTHON5000WriteReg(PLL_CONFIG, 0x0003);      // enable PLL and set to operational
	PYTHON5000WriteReg(SOFT_RESET_PLL, 0x0090);  // reset PLL lock detect
	PYTHON5000WriteReg(SOFT_RESET_PLL, 0x0000);  // release PLL lock detect reset

	while(!(PYTHON5000ReadReg(PLL_LOCK) & PLL_LOCK_INDICATION_MASK)); // wait for PLL lock indication  MAKE SURE TO UNCOMMENT WHEN TESTING HARDWARE

	PYTHON5000WriteReg(SOFT_RESET_CGEN, 0x0000); // release clock generator soft reset

	#ifdef MUX8
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0006);    // mux 8 data-channel
	#elif defined MUX4
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0016);    // mux 4 data-channel
	#elif defined MUX2
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0026);    // mux 2 data-channel
	#elif defined MUX1
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0036);    // mux 1 data-channel
	#endif

	PYTHON5000WriteReg(CLOCK_ENABLE, 0x01); // enable logic clock
}

/**
  * @brief  Exexutes required register uploads to get an
  *         image out. (apparently)
  * @param  None
  * @retval None
  */
void PYTHON5000InitializeCoreRegisters() {

	PYTHON5000WriteReg(IMAGE_CORE_CONFIG1, 0x0854);   // idk what its doing here, leave it for now
	PYTHON5000WriteReg(42, 0x0200);
	PYTHON5000WriteReg(43, 0x000C);
	PYTHON5000WriteReg(BIAS_CONFIG, 0x48CB);

	#ifdef MUX8 
		PYTHON5000WriteReg(AFE_BIAS_CONFIG, 0x53C8);      // set AEF bias
		PYTHON5000WriteReg(COLUMN_BIAS_CONFIG, 0x8688);   // set mux bias
		PYTHON5000WriteReg(ADC_BIAS, 0x0888);             // set ADC bias
	  
	#elif defined MUX4 
		PYTHON5000WriteReg(AFE_BIAS_CONFIG, 0x53C4);      // set AEF bias
		PYTHON5000WriteReg(COLUMN_BIAS_CONFIG, 0x4544);   // set mux bias
		PYTHON5000WriteReg(ADC_BIAS, 0x0848);             // set ADC bias
	
	#elif defined MUX2 
		PYTHON5000WriteReg(AFE_BIAS_CONFIG, 0x53C2);      // set AEF bias
		PYTHON5000WriteReg(COLUMN_BIAS_CONFIG, 0x2322);   // set mux bias
		PYTHON5000WriteReg(ADC_BIAS, 0x0828);             // set ADC bias
	
	#elif defined MUX1 
		PYTHON5000WriteReg(AFE_BIAS_CONFIG, 0x53C1);      // set AEF bias
		PYTHON5000WriteReg(COLUMN_BIAS_CONFIG, 0x1211);   // set mux bias
		PYTHON5000WriteReg(ADC_BIAS, 0x0818);             // set ADC bias
	#endif

	PYTHON5000WriteReg(LVDS_BIAS_CONFIG, 0x0085);     // set LVDS bias
	PYTHON5000WriteReg(70, 0x4411);                   // portal said to do it
	PYTHON5000WriteReg(71, 0x9788);                   // portal said to do it
	PYTHON5000WriteReg(CHARGE_PUMP_CONFIG, 0x3330);   // portal said to do it
	PYTHON5000WriteReg(BLACK_CAL, 0x4714);            // set black offset

	PYTHON5000WriteReg(TEMP_SENSOR_CONFIG, 0x0001);

	PYTHON5000WriteReg(GENERAL_CONFIG, 0x8001);       
	PYTHON5000WriteReg(SEQ_GEN_CONFIG, 0x2C0C);       // Disable sequencer, enable trigger mode, set monitor to 0x04
	PYTHON5000WriteReg(INTEGRATION_CONTROL, 0x03E4);  // sets integration control

	/*overhead lines configuration*/
	PYTHON5000WriteReg(BLACK_LINE_CONFIG, 0x0104);    // Black lines (3 black lines, one gated)
	PYTHON5000WriteReg(198, 0x0000); 				// dummy lines
	PYTHON5000WriteReg(REF_LINES, 0x0000);            // Reference lines (gray lines)

	PYTHON5000WriteReg(EXPOSURE0, 0x0064);            // initialize exposure
	PYTHON5000WriteReg(232, 0x0064);                  // they have this, but reg 232 doesnt exist

	#ifdef MUX8 
		PYTHON5000WriteReg(SEQ_DEL_CONFIG, 0x4E00);       // XSM delay
		PYTHON5000WriteReg(MULT_TIMER_0, 0x02C9);         // Mult_timer_0
		PYTHON5000WriteReg(230, 0x02C9);                // Mult_timer_1     I dont think we need this
		PYTHON5000WriteReg(FR_LENGTH0, 0x0494);           // Fr_length_0
		PYTHON5000WriteReg(231, 0x0494);                // Fr_length_1      I dont think we need this
	
	#elif defined MUX4 
		PYTHON5000WriteReg(SEQ_DEL_CONFIG, 0x2C00);       // XSM delay
		PYTHON5000WriteReg(MULT_TIMER_0, 0x0165);         // Mult_timer_0
		PYTHON5000WriteReg(230, 0x0165);                // Mult_timer_1     I dont think we need this
		PYTHON5000WriteReg(FR_LENGTH0, 0x0866);           // Fr_length_0
		PYTHON5000WriteReg(231, 0x0866);                // Fr_length_1      I dont think we need this
	
	#elif defined MUX2 
		PYTHON5000WriteReg(SEQ_DEL_CONFIG, 0x1C00);       // XSM delay
		PYTHON5000WriteReg(MULT_TIMER_0, 0x00B2);         // Mult_timer_0
		PYTHON5000WriteReg(230, 0x00B2);                // Mult_timer_1     I dont think we need this
		PYTHON5000WriteReg(FR_LENGTH0, 0x1018);           // Fr_length_0
		PYTHON5000WriteReg(231, 0x1018);                // Fr_length_1      I dont think we need this
	
	#elif defined MUX1 
		PYTHON5000WriteReg(SEQ_DEL_CONFIG, 0x1400);       // XSM delay
		PYTHON5000WriteReg(MULT_TIMER_0, 0x0059);         // Mult_timer_0
		PYTHON5000WriteReg(230, 0x0059);                // Mult_timer_1     I dont think we need this
		PYTHON5000WriteReg(FR_LENGTH0, 0x1F72);           // Fr_length_0
		PYTHON5000WriteReg(231, 0x1F72);                // Fr_length_1      I dont think we need this
	#endif

	/*analog and digital gain config*/
	PYTHON5000WriteReg(ANALOG_GAIN, 0x01E1);          	// Analog_gain_0 ([12:5]: AFE_gain // [4:0]: MUX_gain)
	PYTHON5000WriteReg(235, 0x01e1);                    // Analog_gain_1 ([12:5]: AFE_gain // [4:0]: MUX_gain)
	PYTHON5000WriteReg(DIGITAL_GAIN, 0x0080);	          // Digital_gain_0
	PYTHON5000WriteReg(236, 0x0080);                      // Digital_gain_1

	// PYTHON5000WriteReg(ACTIVE_ROI, 0x0001);                // disable all ROI
	// PYTHON5000WriteReg(ACTIVE_ROI1, 0x0001);
	PYTHON5000WriteReg(256, 0x9F00);
	PYTHON5000WriteReg(257, 0x0000);
	PYTHON5000WriteReg(258, 0x03FF);

     #ifdef MUX8
		PYTHON5000WriteReg(211, 0x0e49);
	#elif defined MUX4
		PYTHON5000WriteReg(211, 0x0e39);
	#elif defined MUX2
		PYTHON5000WriteReg(211, 0x0e29);
	#elif defined MUX1
		PYTHON5000WriteReg(211, 0x0e19);
	#endif

     /*Timing Configuration*/
	PYTHON5000WriteReg(215, 0x111f);
	PYTHON5000WriteReg(216, 0x7f00);
	PYTHON5000WriteReg(219, 0x0020);
	PYTHON5000WriteReg(224, 0x3e17);
	PYTHON5000WriteReg(227, 0x0000);
	PYTHON5000WriteReg(250, 0x2081);

	
	PYTHON5000WriteReg(384, 0xC800);
	PYTHON5000WriteReg(385, 0xFB1F);
	PYTHON5000WriteReg(386, 0xFB1F);
	PYTHON5000WriteReg(387, 0xFB12);
	PYTHON5000WriteReg(388, 0xF912);
	PYTHON5000WriteReg(389, 0xF902);
	PYTHON5000WriteReg(390, 0xF804);
	PYTHON5000WriteReg(391, 0xF008);
	PYTHON5000WriteReg(392, 0xF102);
	PYTHON5000WriteReg(393, 0xF30F);
	PYTHON5000WriteReg(394, 0xF30F);
	PYTHON5000WriteReg(395, 0xF30F);
	PYTHON5000WriteReg(396, 0xF30F);
	PYTHON5000WriteReg(397, 0xF30F);
	PYTHON5000WriteReg(398, 0xF30F);
	PYTHON5000WriteReg(399, 0xF102);
	PYTHON5000WriteReg(400, 0xF008);
	PYTHON5000WriteReg(401, 0xF24A);
	PYTHON5000WriteReg(402, 0xF264);
	PYTHON5000WriteReg(403, 0xF226);
	PYTHON5000WriteReg(404, 0xF021);
	PYTHON5000WriteReg(405, 0xF002);
	PYTHON5000WriteReg(406, 0xF40A);
	PYTHON5000WriteReg(407, 0xF005);
	PYTHON5000WriteReg(408, 0xF20F);
	PYTHON5000WriteReg(409, 0xF20F);
	PYTHON5000WriteReg(410, 0xF20F);
	PYTHON5000WriteReg(411, 0xF20F);
	PYTHON5000WriteReg(412, 0xF005);
	PYTHON5000WriteReg(413, 0xEC05);
	PYTHON5000WriteReg(414, 0xC801);
	PYTHON5000WriteReg(415, 0xC800);

	PYTHON5000WriteReg(416, 0xc800);
	PYTHON5000WriteReg(417, 0xcc0a);
	PYTHON5000WriteReg(418, 0xc806);
	PYTHON5000WriteReg(419, 0xc800);

	#ifdef MUX8 
		PYTHON5000WriteReg(220, 0x3424);

		PYTHON5000WriteReg(420, 0x0030);
		PYTHON5000WriteReg(421, 0x2079);
		PYTHON5000WriteReg(422, 0x2071);
		PYTHON5000WriteReg(423, 0x0071);
		PYTHON5000WriteReg(424, 0x107f);
		PYTHON5000WriteReg(425, 0x1079);
		PYTHON5000WriteReg(426, 0x0071);
		PYTHON5000WriteReg(427, 0x0031);
		PYTHON5000WriteReg(428, 0x01b4);
		PYTHON5000WriteReg(429, 0x21b9);
		PYTHON5000WriteReg(430, 0x20b1);
		PYTHON5000WriteReg(431, 0x00b1);
		PYTHON5000WriteReg(432, 0x10bf);
		PYTHON5000WriteReg(433, 0x10b9);
		PYTHON5000WriteReg(434, 0x00b1);
		PYTHON5000WriteReg(435, 0x0030);
		
		PYTHON5000WriteReg(436, 0x0030);
		PYTHON5000WriteReg(437, 0x2179);
		PYTHON5000WriteReg(438, 0x2071);
		PYTHON5000WriteReg(439, 0x0071);
		PYTHON5000WriteReg(440, 0x107f);
		PYTHON5000WriteReg(441, 0x1079);
		PYTHON5000WriteReg(442, 0x0071);
		PYTHON5000WriteReg(443, 0x0031);
		PYTHON5000WriteReg(444, 0x01b4);
		PYTHON5000WriteReg(445, 0x21b9);
		PYTHON5000WriteReg(446, 0x20b1);
		PYTHON5000WriteReg(447, 0x00b1);
		PYTHON5000WriteReg(448, 0x10bf);
		PYTHON5000WriteReg(449, 0x10b9);
		PYTHON5000WriteReg(450, 0x00b1);
		PYTHON5000WriteReg(451, 0x0030);
		
		PYTHON5000WriteReg(452, 0x0030);
		PYTHON5000WriteReg(453, 0x2079);
		PYTHON5000WriteReg(454, 0x2071);
		PYTHON5000WriteReg(455, 0x0071);
		PYTHON5000WriteReg(456, 0x1077);
		PYTHON5000WriteReg(457, 0x0071);
		PYTHON5000WriteReg(458, 0x0031);
		PYTHON5000WriteReg(459, 0x01b4);
		PYTHON5000WriteReg(460, 0x21b9);
		PYTHON5000WriteReg(461, 0x20b1);
		PYTHON5000WriteReg(462, 0x00b1);
		PYTHON5000WriteReg(463, 0x10bf);
		PYTHON5000WriteReg(464, 0x10bf);
		PYTHON5000WriteReg(465, 0x10ba);
		PYTHON5000WriteReg(466, 0x00b1);
		PYTHON5000WriteReg(467, 0x0030);
    
	#elif defined MUX4 
		
		PYTHON5000WriteReg(220, 0x3224);
		
		PYTHON5000WriteReg(420, 0x0030);
		PYTHON5000WriteReg(421, 0x2075);
		PYTHON5000WriteReg(422, 0x2071);
		PYTHON5000WriteReg(423, 0x0071);
		PYTHON5000WriteReg(424, 0x107c);
		PYTHON5000WriteReg(425, 0x0071);
		PYTHON5000WriteReg(426, 0x0031);
		PYTHON5000WriteReg(427, 0x01b2);
		PYTHON5000WriteReg(428, 0x21b5);
		PYTHON5000WriteReg(429, 0x20b1);
		PYTHON5000WriteReg(430, 0x00b1);
		PYTHON5000WriteReg(431, 0x10bc);
		PYTHON5000WriteReg(432, 0x00b1);
		PYTHON5000WriteReg(433, 0x0030);
		
		PYTHON5000WriteReg(434, 0x0030);

		PYTHON5000WriteReg(435, 0x2175);
		PYTHON5000WriteReg(436, 0x2071);
		PYTHON5000WriteReg(437, 0x0071);
		PYTHON5000WriteReg(438, 0x107c);
		PYTHON5000WriteReg(439, 0x0071);
		PYTHON5000WriteReg(440, 0x0031);
		PYTHON5000WriteReg(441, 0x01b2);
		PYTHON5000WriteReg(442, 0x21b5);
		PYTHON5000WriteReg(443, 0x20b1);
		PYTHON5000WriteReg(444, 0x00b1);
		PYTHON5000WriteReg(445, 0x10bc);
		PYTHON5000WriteReg(446, 0x00b1);
		PYTHON5000WriteReg(447, 0x0030);
		
		PYTHON5000WriteReg(448, 0x0030);
		PYTHON5000WriteReg(449, 0x2075);
		PYTHON5000WriteReg(450, 0x2071);
		PYTHON5000WriteReg(451, 0x0071);
		PYTHON5000WriteReg(452, 0x1073);
		PYTHON5000WriteReg(453, 0x0071);
		PYTHON5000WriteReg(454, 0x0031);
		PYTHON5000WriteReg(455, 0x01b2);
		PYTHON5000WriteReg(456, 0x21b5);
		PYTHON5000WriteReg(457, 0x20b1);
		PYTHON5000WriteReg(458, 0x00b1);
		PYTHON5000WriteReg(459, 0x10bf);
		PYTHON5000WriteReg(460, 0x10ba);
		PYTHON5000WriteReg(461, 0x00b1);
		PYTHON5000WriteReg(462, 0x0030);
    
	#elif defined MUX2 
		
		PYTHON5000WriteReg(220, 0x3224);
				
		PYTHON5000WriteReg(420, 0x0030);
		PYTHON5000WriteReg(421, 0x2073);
		PYTHON5000WriteReg(422, 0x2071);
		PYTHON5000WriteReg(423, 0x0071);
		PYTHON5000WriteReg(424, 0x1076);
		PYTHON5000WriteReg(425, 0x0071);
		PYTHON5000WriteReg(426, 0x0031);
		PYTHON5000WriteReg(427, 0x01b2);
		PYTHON5000WriteReg(428, 0x21b3);
		PYTHON5000WriteReg(429, 0x20b1);
		PYTHON5000WriteReg(430, 0x00b1);
		PYTHON5000WriteReg(431, 0x10b6);
		PYTHON5000WriteReg(432, 0x00b1);
		PYTHON5000WriteReg(433, 0x0030);
		
		PYTHON5000WriteReg(434, 0x0030);
		PYTHON5000WriteReg(435, 0x2173);
		PYTHON5000WriteReg(436, 0x2071);
		PYTHON5000WriteReg(437, 0x0071);
		PYTHON5000WriteReg(438, 0x1076);
		PYTHON5000WriteReg(439, 0x0071);
		PYTHON5000WriteReg(440, 0x0031);
		PYTHON5000WriteReg(441, 0x01b2);
		PYTHON5000WriteReg(442, 0x21b3);
		PYTHON5000WriteReg(443, 0x20b1);
		PYTHON5000WriteReg(444, 0x00b1);
		PYTHON5000WriteReg(445, 0x10b6);
		PYTHON5000WriteReg(446, 0x00b1);
		PYTHON5000WriteReg(447, 0x0030);
		
		PYTHON5000WriteReg(448, 0x0030);
		PYTHON5000WriteReg(449, 0x2073);
		PYTHON5000WriteReg(450, 0x2071);
		PYTHON5000WriteReg(451, 0x0071);
		PYTHON5000WriteReg(452, 0x1071);
		PYTHON5000WriteReg(453, 0x0071);
		PYTHON5000WriteReg(454, 0x0031);
		PYTHON5000WriteReg(455, 0x01b2);
		PYTHON5000WriteReg(456, 0x21b3);
		PYTHON5000WriteReg(457, 0x20b1);
		PYTHON5000WriteReg(458, 0x00b1);
		PYTHON5000WriteReg(459, 0x10b4);
		PYTHON5000WriteReg(460, 0x00b1);
		PYTHON5000WriteReg(461, 0x0030);
    	
	#elif defined MUX1 

		PYTHON5000WriteReg(220, 0x3224);
				
		PYTHON5000WriteReg(420, 0x0030);
		PYTHON5000WriteReg(421, 0x2072);
		PYTHON5000WriteReg(422, 0x2071);
		PYTHON5000WriteReg(423, 0x0071);
		PYTHON5000WriteReg(424, 0x1073);
		PYTHON5000WriteReg(425, 0x0071);
		PYTHON5000WriteReg(426, 0x0031);
		PYTHON5000WriteReg(427, 0x01b1);
		PYTHON5000WriteReg(428, 0x21b2);
		PYTHON5000WriteReg(429, 0x20b1);
		PYTHON5000WriteReg(430, 0x00b1);
		PYTHON5000WriteReg(431, 0x10b3);
		PYTHON5000WriteReg(432, 0x00b1);
		PYTHON5000WriteReg(433, 0x0030);
		
		PYTHON5000WriteReg(434, 0x0030);
		PYTHON5000WriteReg(435, 0x2172);
		PYTHON5000WriteReg(436, 0x2071);
		PYTHON5000WriteReg(437, 0x0071);
		PYTHON5000WriteReg(438, 0x1073);
		PYTHON5000WriteReg(439, 0x0071);
		PYTHON5000WriteReg(440, 0x0031);
		PYTHON5000WriteReg(441, 0x01b1);
		PYTHON5000WriteReg(442, 0x21b2);
		PYTHON5000WriteReg(443, 0x20b1);
		PYTHON5000WriteReg(444, 0x00b1);
		PYTHON5000WriteReg(445, 0x10b3);
		PYTHON5000WriteReg(446, 0x00b1);
		PYTHON5000WriteReg(447, 0x0030);
		
		PYTHON5000WriteReg(448, 0x0030);
		PYTHON5000WriteReg(449, 0x2072);
		PYTHON5000WriteReg(450, 0x2071);
		PYTHON5000WriteReg(451, 0x0071);
		PYTHON5000WriteReg(452, 0x1071);
		PYTHON5000WriteReg(453, 0x0071);
		PYTHON5000WriteReg(454, 0x0031);
		PYTHON5000WriteReg(455, 0x01b1);
		PYTHON5000WriteReg(456, 0x21b2);
		PYTHON5000WriteReg(457, 0x20b1);
		PYTHON5000WriteReg(458, 0x00b1);
		PYTHON5000WriteReg(459, 0x10bf);
		PYTHON5000WriteReg(460, 0x10bf);
		PYTHON5000WriteReg(461, 0x00b1);
		PYTHON5000WriteReg(462, 0x0030);
     #endif

}

/**
  * @brief  Powers up all required blocks
  * @param  None
  * @retval None
  */
void PYTHON5000SoftPowerUp() {
	#ifdef MUX8
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0007);    // mux 8 data-channel and enable analog clock
	#elif defined MUX4
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0017);    // mux 4 data-channel and enable analog clock
	#elif defined MUX2
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0027);    // mux 2 data-channel and enable analog clock
	#elif defined MUX1
		PYTHON5000WriteReg(CLOCK_CONFIG, 0x0037);    // mux 1 data-channel and enable analog clock
	#endif

	PYTHON5000WriteReg(BIAS_PWR_CONFIG, 0x0001);      // enable bias block
	PYTHON5000WriteReg(LVDS_CONFIG, 0x0007);          // enable LVDS transmitters
	PYTHON5000WriteReg(SOFT_RESET_ANALOG, 0x0000);    // enable analog block
	PYTHON5000WriteReg(42, 0x0203);
	PYTHON5000WriteReg(IMAGE_CORE_CONFIG0, 0x0003);   // power up image core
	PYTHON5000WriteReg(AFE_CONFIG, 0x0001);           // power up AFE
	PYTHON5000WriteReg(CHARGE_PUMP_CONFIG, 0x3337);   //enable charge pump
}

/**
  * @brief  Enables PYTHON5000 sequencer
  * @param  None
  * @retval None
  */
void PYTHON5000EnableSquencer() {
	uint16_t buf;

	PYTHON5000WriteReg(MULT_TIMER_0, 0x0007);
	PYTHON5000WriteReg(FR_LENGTH0, 0xFFFF);
	PYTHON5000WriteReg(EXPOSURE0, 0x03D0);

	buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
	buf |= SEQUENCER_ENABLE_MASK;
	PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf); 

	//PYTHON5000WriteReg(FR_LENGTH0, 0x0866);       
	PYTHON5000WriteReg(MULT_TIMER_0, 0x00FF);         // Mult_timer_0
	PYTHON5000WriteReg(EXPOSURE0, 0x0064);

}

/**
  * @brief  Disables PYTHON5000 sequencer
  * @param  None
  * @retval None
  */
void PYTHON5000DisableSequencer() {
     uint16_t buf;
     buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
     buf &= ~(uint16_t)SEQUENCER_ENABLE_MASK;
     PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf);
}

/**
  * @brief  Enables PYTHON5000 subsampling
  * @param  None
  * @retval None
  */
void PYTHON5000EnableSubsampling() {
     uint16_t buf;
     buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
     buf |= SUBSAMPLING_MASK;
     PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf); 
}

/**
  * @brief  Disables PYTHON5000 subsampling
  * @param  None
  * @retval None
  */
void PYTHON5000DisableSubsampling() {
     uint16_t buf;
     buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
     buf &= ~(uint16_t)SUBSAMPLING_MASK;
     PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf);
}

/**
  * @brief  Enables selected PYTHON5000 ROI
  * @param  None
  * @retval None
  */
void PYTHON5000EnableROI(uint16_t roi) {
	uint16_t reg = 0x01 << roi;
	
	PYTHON5000WriteReg(ACTIVE_ROI, reg);
}

/**
  * @brief  Disables all PYTHON5000 ROIs
  * @param  None
  * @retval None
  */
void PYTHON5000DisableROI() {
     PYTHON5000WriteReg(ACTIVE_ROI, 0x0000);
}

/**
  * @brief  Enables PYTHON5000 Trigger mode
  * @param  None
  * @retval None
  */
void PYTHON5000EnableTrigger() {
	uint16_t buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
	buf |= TRIGGER_ENABLE_MASK;
	PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf);
	trigger = 1;
}

/**
  * @brief  Disables PYTHON5000 Trigger mode
  * @param  None
  * @retval None
  */
void PYTHON5000DisableTrigger() {
	uint16_t buf = PYTHON5000ReadReg(SEQ_GEN_CONFIG);
	buf &= ~(uint16_t)TRIGGER_ENABLE_MASK;
	PYTHON5000WriteReg(SEQ_GEN_CONFIG, buf);
	trigger = 0;
}

/**
  * @brief  Puts test patterns on data lines
  * @param  none
  * @retval none
  */
void PYTHON5000EnableTestPattern() {
	PYTHON5000WriteReg(TEST_PAT_CONFIG, 0x0009);
}

/**
  * @brief  Disables test patterns on data lines
  * @param  none
  * @retval none
  */
void PYTHON5000DisableTestPattern() {
	PYTHON5000WriteReg(TEST_PAT_CONFIG, 0x0000);
}

/**
  * @brief  Enables PYTHON5000 automatic exposure control (AEC)
  * @param  none
  * @retval none
  */
void PYTHON5000EnableAEC() {
	uint16_t buf = PYTHON5000ReadReg(AEC_CONFIG);
	buf |= (uint16_t)AEC_ENABLE_MASK;
	PYTHON5000WriteReg(AEC_INTENSITY, 0x61F0);
	PYTHON5000WriteReg(AEC_CONFIG, buf);
}

/**
  * @brief  Disables PYTHON5000 automatic exposure control (AEC)
  * @param  none
  * @retval none
  */
void PYTHON5000DisableAEC() {
	uint16_t buf = PYTHON5000ReadReg(AEC_CONFIG);
	buf &= ~(uint16_t)AEC_ENABLE_MASK;
	PYTHON5000WriteReg(AEC_CONFIG, buf);
}

/**
  * @brief  Sets PYTHON5000 region of interest for exposure control
  * 		  - should the the same as the active ROI
  * @param  none
  * @retval none
  */
void PYTHON5000SetAECROI(uint16_t xstart, uint16_t ystart, uint16_t xend, uint16_t yend) {
	uint16_t x;
	x = xend << 8 | xstart;

	PYTHON5000WriteReg(AEC_ROI_X, x);
	PYTHON5000WriteReg(AEC_ROI_Y_START, ystart);
	PYTHON5000WriteReg(AEC_ROI_Y_END , yend);
}

/**
  * @brief  Sets PYTHON5000 region of interest (max 16K pixels)
  * @param  none
  * @retval none
  */
void PYTHON5000SetROI(uint8_t roi, uint16_t xstart, uint16_t ystart, uint16_t xend, uint16_t yend) {
	uint16_t x;
	x = xend << 8 | xstart;

	PYTHON5000WriteReg(ROI0_X_CONFIG + roi * ROI_REG_OFFSET, x);
	PYTHON5000WriteReg(ROI0_Y_START + roi * ROI_REG_OFFSET, ystart);
	PYTHON5000WriteReg(ROI0_Y_END + roi * ROI_REG_OFFSET, yend);
}

/**
  * @brief  Aligns LVDS data
  * @param  none
  * @retval none
  */
void PYTHON5000AlignData() {
	volatile uint8_t testpattern;

	testpattern = (PYTHON5000ReadReg(TRAINING_PATTERN) & 0x03FF) >> 2; //only using 8 bits
	PYTHON5000EnableTestPattern();
	ImageMemWriteEnable();

	while(*image_mem_base != testpattern) {
		GPIOWriteBit(BITSLIP, GPIO_HIGH);
		GPIOWriteBit(BITSLIP, GPIO_LOW);
	}

	ImageMemWriteDisable();
	PYTHON5000DisableTestPattern();
}


/**
  * @brief  Triggers PYTHON to take photo
  * @param  none
  * @retval none
  */
uint8_t PYTHON5000Capture() {
	volatile uint16_t status;
	volatile uint32_t tickstart;
	volatile uint8_t *image_ptr = image_mem_base;

	//PYTHON5000SetExposure();
	
	// initialize image memory
	while(image_ptr < (uint8_t *)(IMAGE_DATA_MEM_SPAN + IMAGE_DATA_MEM_BASE)) {
		*image_ptr++ = 0;
	}

	ImageMemWriteEnable();
	EnableAddressCounter();
	if(trigger)
		Trigger();

	tickstart = alt_nticks();
	do {
		status = PYTHON5000GetStatus();
		if(alt_nticks() - tickstart > CAPTURE_TIMEOUT)
			return 0;
	} while(!(status & READING_IMAGE));

	tickstart = alt_nticks();
	do {
		status = PYTHON5000GetStatus();
		if(alt_nticks() - tickstart > CAPTURE_TIMEOUT)
			return 0;
	} while(status & READING_IMAGE);

	ImageMemWriteDisable();
	arrangeImage();
	PYTHON5000UpdateStatus();
	ResetAddressCounter();

	return 1;
}

/**
  * @brief  Automatically sets exposure (AEC must be enabled)
  * @param  none
  * @retval none
  */
void PYTHON5000SetExposure() {
	//while(!(PYTHON5000ReadReg(AEC_STATUS) & AEC_LOCKED));
}

/**
  * @brief  Writes image to UART
  * @param  none
  * @retval none
  */
void displayImage() {
	uint8_t *pixel = image_mem_base;
	uint8_t *image_end = image_mem_end;

	while(pixel != image_end) {
		sendByte(*pixel++);
	}	
}

/**
  * @brief  Gets the temperature of the python
  * @param  none
  * @retval temperature value
  */
uint32_t PYTHON5000GetTemp() {
	uint8_t temp;
	PYTHON5000WriteReg(TEMP_SENSOR_CONFIG, 0x0001);
	temp = (uint8_t)PYTHON5000ReadReg(TEMP_SENSOR_STATUS);

	return temp * 3 / 4;
}



