#ifndef STATE_OVERHEATING_H_
#define STATE_OVERHEATING_H_

#include <state.h>

void OverheatingEnterHandler(SystemState *systemState);
void OverheatingProcessHandler(SystemState *systemState);
void OverheatingExitHandler(SystemState *systemState);

#endif