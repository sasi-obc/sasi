#ifndef STATE_COMMS_H_
#define STATE_COMMS_H_

#include <state.h>

void CommsProcessHandler(SystemState *systemState);
void CommsExitHandler(SystemState *systemState);

#endif