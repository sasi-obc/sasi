#ifndef STATE_CAPTURE_H_
#define STATE_CAPTURE_H_

#include <types.h>
#include <state.h>

void CaptureEnterHandler(SystemState *systemState);
void CaptureProcessHandler(SystemState *systemState);
void CaptureExitHandler(SystemState *systemState);

#endif