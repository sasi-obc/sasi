#include <state_comms.h>
#include <command.h>

void CommsProcessHandler(SystemState *systemState) {
     ChangeState(systemState, STATE_IDLE);
}

void CommsExitHandler(SystemState *systemState) {
     SendCmd(COMMS_XFER_COMPLETE);
}