#include <state_overheating.h>
#include <gpio.h>
#include <python5000.h>

void OverheatingEnterHandler(SystemState *systemState) {
     GPIOWriteBit(LVDS_PLL_RESET, GPIO_HIGH);
     PYTHON5000DisableSequencer();
}

void OverheatingProcessHandler(SystemState *systemState) {
     while(systemState->temperature > 78) {
          systemState->temperature = PYTHON5000GetTemp();
     }

     ChangeState(systemState, STATE_INIT);
}

void OverheatingExitHandler(SystemState *systemState) {
     GPIOWriteBit(LVDS_PLL_RESET, GPIO_LOW);
}