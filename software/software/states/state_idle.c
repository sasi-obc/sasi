#include <state_idle.h>
#include <python5000.h>
#include <can.h>

void IdleProcessHandler(SystemState *systemState) {
     uint8_t cmd = MAILBOX_EMPTY; 
     systemState->temperature = PYTHON5000GetTemp();
     if(systemState->temperature > 78)
          cmd = OVERHEATING;
     else if(checkMailbox())
          cmd = ReceiveCmd();

     switch(cmd) {
          case CAPTURE_IMAGE : 
               ChangeState(systemState, STATE_CAPTURE);
               break;
          
          case SEND_TO_COMMS :
               ChangeState(systemState, STATE_COMMS);
               break;

          case MAILBOX_EMPTY :
               break;

          case OVERHEATING   :
               ChangeState(systemState, STATE_OVERHEATING);
               break;

          default:
               break;
     }
}
