#ifndef STATE_IDLE_H_
#define STATE_IDLE_H_

#include <types.h>
#include <state.h>

void IdleProcessHandler(SystemState *systemState);

#endif