#include <state_capture.h>
#include <python5000.h>
#include <MX25L256.h>
#include <command.h>

void CaptureEnterHandler(SystemState *systemState) {
     systemState->image_data_valid = 0;
}

void CaptureProcessHandler(SystemState *systemState) {

     systemState->image_data_valid = PYTHON5000Capture();
     systemState->image_line_length = PYTHON5000GetLineLength();
     systemState->image_size = PYTHON5000GetImageSize();

     #ifdef DISPLAY
          ChangeState(systemState, STATE_DISPLAY);
     #else
          ChangeState(systemState, STATE_IDLE);
     #endif
}

void CaptureExitHandler(SystemState *systemState) {

     if(systemState->image_data_valid) {
          FlashSaveImage();
          SendCmd(IMAGE_CAPTURED); 
     }
     else      
          SendCmd(IMAGE_CAPTURE_FAIL);
}
