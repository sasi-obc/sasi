#include <state_init.h>
#include <uart.h>
#include <python5000.h>
#include <can.h>
#include <MX25L256.h>

void InitProcessHandler(SystemState *systemState) {
     uartInit();
	PYTHON5000Init();
	CANInit();
	MX25L256Init();
	FlashLoadFileHeader();
	ChangeState(systemState, STATE_IDLE);
}
