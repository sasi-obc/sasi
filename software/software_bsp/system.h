/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'cpu' in SOPC Builder design 'processor'
 * SOPC Builder design path: ../../processor.sopcinfo
 *
 * Generated: Wed Apr 01 12:50:14 ADT 2020
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CAN_REG configuration
 *
 */

#define ALT_MODULE_CLASS_CAN_REG altera_avalon_onchip_memory2
#define CAN_REG_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define CAN_REG_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define CAN_REG_BASE 0x16120
#define CAN_REG_CONTENTS_INFO ""
#define CAN_REG_DUAL_PORT 1
#define CAN_REG_GUI_RAM_BLOCK_TYPE "AUTO"
#define CAN_REG_INIT_CONTENTS_FILE "processor_CAN_REG"
#define CAN_REG_INIT_MEM_CONTENT 0
#define CAN_REG_INSTANCE_ID "NONE"
#define CAN_REG_IRQ -1
#define CAN_REG_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CAN_REG_NAME "/dev/CAN_REG"
#define CAN_REG_NON_DEFAULT_INIT_FILE_ENABLED 0
#define CAN_REG_RAM_BLOCK_TYPE "AUTO"
#define CAN_REG_READ_DURING_WRITE_MODE "DONT_CARE"
#define CAN_REG_SINGLE_CLOCK_OP 0
#define CAN_REG_SIZE_MULTIPLE 1
#define CAN_REG_SIZE_VALUE 32
#define CAN_REG_SPAN 32
#define CAN_REG_TYPE "altera_avalon_onchip_memory2"
#define CAN_REG_WRITABLE 1


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_gen2"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x00015820
#define ALT_CPU_CPU_ARCH_NIOS2_R1
#define ALT_CPU_CPU_FREQ 50000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "tiny"
#define ALT_CPU_DATA_ADDR_WIDTH 0x11
#define ALT_CPU_DCACHE_LINE_SIZE 0
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_DCACHE_SIZE 0
#define ALT_CPU_EXCEPTION_ADDR 0x00008020
#define ALT_CPU_FLASH_ACCELERATOR_LINES 0
#define ALT_CPU_FLASH_ACCELERATOR_LINE_SIZE 0
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 50000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 0
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 0
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 0
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 0
#define ALT_CPU_ICACHE_SIZE 0
#define ALT_CPU_INST_ADDR_WIDTH 0x11
#define ALT_CPU_NAME "cpu"
#define ALT_CPU_OCI_VERSION 1
#define ALT_CPU_RESET_ADDR 0x00008000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x00015820
#define NIOS2_CPU_ARCH_NIOS2_R1
#define NIOS2_CPU_FREQ 50000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "tiny"
#define NIOS2_DATA_ADDR_WIDTH 0x11
#define NIOS2_DCACHE_LINE_SIZE 0
#define NIOS2_DCACHE_LINE_SIZE_LOG2 0
#define NIOS2_DCACHE_SIZE 0
#define NIOS2_EXCEPTION_ADDR 0x00008020
#define NIOS2_FLASH_ACCELERATOR_LINES 0
#define NIOS2_FLASH_ACCELERATOR_LINE_SIZE 0
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 0
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 0
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_ILLEGAL_INSTRUCTION_EXCEPTION
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 0
#define NIOS2_ICACHE_LINE_SIZE_LOG2 0
#define NIOS2_ICACHE_SIZE 0
#define NIOS2_INST_ADDR_WIDTH 0x11
#define NIOS2_OCI_VERSION 1
#define NIOS2_RESET_ADDR 0x00008000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ALTERA_AVALON_EPCS_FLASH_CONTROLLER
#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PERFORMANCE_COUNTER
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SPI
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_AVALON_UART
#define __ALTERA_NIOS2_GEN2
#define __ALTPLL


/*
 * IMAGE_DATA_MEM configuration
 *
 */

#define ALT_MODULE_CLASS_IMAGE_DATA_MEM altera_avalon_onchip_memory2
#define IMAGE_DATA_MEM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define IMAGE_DATA_MEM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define IMAGE_DATA_MEM_BASE 0x10000
#define IMAGE_DATA_MEM_CONTENTS_INFO ""
#define IMAGE_DATA_MEM_DUAL_PORT 1
#define IMAGE_DATA_MEM_GUI_RAM_BLOCK_TYPE "AUTO"
#define IMAGE_DATA_MEM_INIT_CONTENTS_FILE "processor_IMAGE_DATA_MEM"
#define IMAGE_DATA_MEM_INIT_MEM_CONTENT 0
#define IMAGE_DATA_MEM_INSTANCE_ID "NONE"
#define IMAGE_DATA_MEM_IRQ -1
#define IMAGE_DATA_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define IMAGE_DATA_MEM_NAME "/dev/IMAGE_DATA_MEM"
#define IMAGE_DATA_MEM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define IMAGE_DATA_MEM_RAM_BLOCK_TYPE "AUTO"
#define IMAGE_DATA_MEM_READ_DURING_WRITE_MODE "DONT_CARE"
#define IMAGE_DATA_MEM_SINGLE_CLOCK_OP 0
#define IMAGE_DATA_MEM_SIZE_MULTIPLE 1
#define IMAGE_DATA_MEM_SIZE_VALUE 16384
#define IMAGE_DATA_MEM_SPAN 16384
#define IMAGE_DATA_MEM_TYPE "altera_avalon_onchip_memory2"
#define IMAGE_DATA_MEM_WRITABLE 1


/*
 * PLL configuration
 *
 */

#define ALT_MODULE_CLASS_PLL altpll
#define PLL_BASE 0x16170
#define PLL_IRQ -1
#define PLL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PLL_NAME "/dev/PLL"
#define PLL_SPAN 16
#define PLL_TYPE "altpll"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone IV E"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag"
#define ALT_STDERR_BASE 0x16188
#define ALT_STDERR_DEV jtag
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag"
#define ALT_STDIN_BASE 0x16188
#define ALT_STDIN_DEV jtag
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag"
#define ALT_STDOUT_BASE 0x16188
#define ALT_STDOUT_DEV jtag
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "processor"


/*
 * Systick configuration
 *
 */

#define ALT_MODULE_CLASS_Systick altera_avalon_timer
#define SYSTICK_ALWAYS_RUN 0
#define SYSTICK_BASE 0x16080
#define SYSTICK_COUNTER_SIZE 32
#define SYSTICK_FIXED_PERIOD 0
#define SYSTICK_FREQ 50000000
#define SYSTICK_IRQ 7
#define SYSTICK_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYSTICK_LOAD_VALUE 49999
#define SYSTICK_MULT 0.001
#define SYSTICK_NAME "/dev/Systick"
#define SYSTICK_PERIOD 1
#define SYSTICK_PERIOD_UNITS "ms"
#define SYSTICK_RESET_OUTPUT 0
#define SYSTICK_SNAPSHOT 1
#define SYSTICK_SPAN 32
#define SYSTICK_TICKS_PER_SEC 1000
#define SYSTICK_TIMEOUT_PULSE_OUTPUT 0
#define SYSTICK_TYPE "altera_avalon_timer"


/*
 * UART configuration
 *
 */

#define ALT_MODULE_CLASS_UART altera_avalon_uart
#define UART_BASE 0x160e0
#define UART_BAUD 115200
#define UART_DATA_BITS 8
#define UART_FIXED_BAUD 1
#define UART_FREQ 72000000
#define UART_IRQ 6
#define UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define UART_NAME "/dev/UART"
#define UART_PARITY 'N'
#define UART_SIM_CHAR_STREAM ""
#define UART_SIM_TRUE_BAUD 0
#define UART_SPAN 32
#define UART_STOP_BITS 1
#define UART_SYNC_REG_DEPTH 2
#define UART_TYPE "altera_avalon_uart"
#define UART_USE_CTS_RTS 0
#define UART_USE_EOP_REGISTER 0


/*
 * epcs_controller configuration
 *
 */

#define ALT_MODULE_CLASS_epcs_controller altera_avalon_epcs_flash_controller
#define EPCS_CONTROLLER_BASE 0x15000
#define EPCS_CONTROLLER_IRQ 4
#define EPCS_CONTROLLER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define EPCS_CONTROLLER_NAME "/dev/epcs_controller"
#define EPCS_CONTROLLER_REGISTER_OFFSET 1024
#define EPCS_CONTROLLER_SPAN 2048
#define EPCS_CONTROLLER_TYPE "altera_avalon_epcs_flash_controller"


/*
 * gpOutput configuration
 *
 */

#define ALT_MODULE_CLASS_gpOutput altera_avalon_pio
#define GPOUTPUT_BASE 0x16100
#define GPOUTPUT_BIT_CLEARING_EDGE_REGISTER 0
#define GPOUTPUT_BIT_MODIFYING_OUTPUT_REGISTER 1
#define GPOUTPUT_CAPTURE 0
#define GPOUTPUT_DATA_WIDTH 13
#define GPOUTPUT_DO_TEST_BENCH_WIRING 0
#define GPOUTPUT_DRIVEN_SIM_VALUE 0
#define GPOUTPUT_EDGE_TYPE "NONE"
#define GPOUTPUT_FREQ 50000000
#define GPOUTPUT_HAS_IN 0
#define GPOUTPUT_HAS_OUT 1
#define GPOUTPUT_HAS_TRI 0
#define GPOUTPUT_IRQ -1
#define GPOUTPUT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define GPOUTPUT_IRQ_TYPE "NONE"
#define GPOUTPUT_NAME "/dev/gpOutput"
#define GPOUTPUT_RESET_VALUE 0
#define GPOUTPUT_SPAN 32
#define GPOUTPUT_TYPE "altera_avalon_pio"


/*
 * hal configuration
 *
 */

#define ALT_INCLUDE_INSTRUCTION_RELATED_EXCEPTION_API
#define ALT_MAX_FD 32
#define ALT_SYS_CLK SYSTICK
#define ALT_TIMESTAMP_CLK HIGH_RES_TIMER


/*
 * high_res_timer configuration
 *
 */

#define ALT_MODULE_CLASS_high_res_timer altera_avalon_timer
#define HIGH_RES_TIMER_ALWAYS_RUN 0
#define HIGH_RES_TIMER_BASE 0x16000
#define HIGH_RES_TIMER_COUNTER_SIZE 64
#define HIGH_RES_TIMER_FIXED_PERIOD 0
#define HIGH_RES_TIMER_FREQ 50000000
#define HIGH_RES_TIMER_IRQ 2
#define HIGH_RES_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define HIGH_RES_TIMER_LOAD_VALUE 2499
#define HIGH_RES_TIMER_MULT 1.0E-6
#define HIGH_RES_TIMER_NAME "/dev/high_res_timer"
#define HIGH_RES_TIMER_PERIOD 50
#define HIGH_RES_TIMER_PERIOD_UNITS "us"
#define HIGH_RES_TIMER_RESET_OUTPUT 0
#define HIGH_RES_TIMER_SNAPSHOT 1
#define HIGH_RES_TIMER_SPAN 64
#define HIGH_RES_TIMER_TICKS_PER_SEC 20000
#define HIGH_RES_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define HIGH_RES_TIMER_TYPE "altera_avalon_timer"


/*
 * jtag configuration
 *
 */

#define ALT_MODULE_CLASS_jtag altera_avalon_jtag_uart
#define JTAG_BASE 0x16188
#define JTAG_IRQ 0
#define JTAG_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_NAME "/dev/jtag"
#define JTAG_READ_DEPTH 64
#define JTAG_READ_THRESHOLD 8
#define JTAG_SPAN 8
#define JTAG_TYPE "altera_avalon_jtag_uart"
#define JTAG_WRITE_DEPTH 64
#define JTAG_WRITE_THRESHOLD 8


/*
 * onchip_mem configuration
 *
 */

#define ALT_MODULE_CLASS_onchip_mem altera_avalon_onchip_memory2
#define ONCHIP_MEM_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define ONCHIP_MEM_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define ONCHIP_MEM_BASE 0x8000
#define ONCHIP_MEM_CONTENTS_INFO ""
#define ONCHIP_MEM_DUAL_PORT 0
#define ONCHIP_MEM_GUI_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEM_INIT_CONTENTS_FILE "processor_onchip_mem"
#define ONCHIP_MEM_INIT_MEM_CONTENT 1
#define ONCHIP_MEM_INSTANCE_ID "NONE"
#define ONCHIP_MEM_IRQ -1
#define ONCHIP_MEM_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ONCHIP_MEM_NAME "/dev/onchip_mem"
#define ONCHIP_MEM_NON_DEFAULT_INIT_FILE_ENABLED 0
#define ONCHIP_MEM_RAM_BLOCK_TYPE "AUTO"
#define ONCHIP_MEM_READ_DURING_WRITE_MODE "DONT_CARE"
#define ONCHIP_MEM_SINGLE_CLOCK_OP 0
#define ONCHIP_MEM_SIZE_MULTIPLE 1
#define ONCHIP_MEM_SIZE_VALUE 19456
#define ONCHIP_MEM_SPAN 19456
#define ONCHIP_MEM_TYPE "altera_avalon_onchip_memory2"
#define ONCHIP_MEM_WRITABLE 1


/*
 * performance_counter configuration
 *
 */

#define ALT_MODULE_CLASS_performance_counter altera_avalon_performance_counter
#define PERFORMANCE_COUNTER_BASE 0x16040
#define PERFORMANCE_COUNTER_HOW_MANY_SECTIONS 3
#define PERFORMANCE_COUNTER_IRQ -1
#define PERFORMANCE_COUNTER_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PERFORMANCE_COUNTER_NAME "/dev/performance_counter"
#define PERFORMANCE_COUNTER_SPAN 64
#define PERFORMANCE_COUNTER_TYPE "altera_avalon_performance_counter"


/*
 * python_status configuration
 *
 */

#define ALT_MODULE_CLASS_python_status altera_avalon_onchip_memory2
#define PYTHON_STATUS_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define PYTHON_STATUS_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define PYTHON_STATUS_BASE 0x16160
#define PYTHON_STATUS_CONTENTS_INFO ""
#define PYTHON_STATUS_DUAL_PORT 1
#define PYTHON_STATUS_GUI_RAM_BLOCK_TYPE "AUTO"
#define PYTHON_STATUS_INIT_CONTENTS_FILE "processor_python_status"
#define PYTHON_STATUS_INIT_MEM_CONTENT 1
#define PYTHON_STATUS_INSTANCE_ID "NONE"
#define PYTHON_STATUS_IRQ -1
#define PYTHON_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PYTHON_STATUS_NAME "/dev/python_status"
#define PYTHON_STATUS_NON_DEFAULT_INIT_FILE_ENABLED 0
#define PYTHON_STATUS_RAM_BLOCK_TYPE "AUTO"
#define PYTHON_STATUS_READ_DURING_WRITE_MODE "DONT_CARE"
#define PYTHON_STATUS_SINGLE_CLOCK_OP 0
#define PYTHON_STATUS_SIZE_MULTIPLE 1
#define PYTHON_STATUS_SIZE_VALUE 16
#define PYTHON_STATUS_SPAN 16
#define PYTHON_STATUS_TYPE "altera_avalon_onchip_memory2"
#define PYTHON_STATUS_WRITABLE 1


/*
 * spi_0 configuration
 *
 */

#define ALT_MODULE_CLASS_spi_0 altera_avalon_spi
#define SPI_0_BASE 0x160c0
#define SPI_0_CLOCKMULT 1
#define SPI_0_CLOCKPHASE 0
#define SPI_0_CLOCKPOLARITY 0
#define SPI_0_CLOCKUNITS "Hz"
#define SPI_0_DATABITS 26
#define SPI_0_DATAWIDTH 32
#define SPI_0_DELAYMULT "1.0E-9"
#define SPI_0_DELAYUNITS "ns"
#define SPI_0_EXTRADELAY 0
#define SPI_0_INSERT_SYNC 0
#define SPI_0_IRQ 3
#define SPI_0_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_0_ISMASTER 1
#define SPI_0_LSBFIRST 0
#define SPI_0_NAME "/dev/spi_0"
#define SPI_0_NUMSLAVES 1
#define SPI_0_PREFIX "spi_"
#define SPI_0_SPAN 32
#define SPI_0_SYNC_REG_DEPTH 2
#define SPI_0_TARGETCLOCK 1000000u
#define SPI_0_TARGETSSDELAY "0.0"
#define SPI_0_TYPE "altera_avalon_spi"


/*
 * spi_1 configuration
 *
 */

#define ALT_MODULE_CLASS_spi_1 altera_avalon_spi
#define SPI_1_BASE 0x160a0
#define SPI_1_CLOCKMULT 1
#define SPI_1_CLOCKPHASE 0
#define SPI_1_CLOCKPOLARITY 0
#define SPI_1_CLOCKUNITS "Hz"
#define SPI_1_DATABITS 8
#define SPI_1_DATAWIDTH 16
#define SPI_1_DELAYMULT "1.0E-9"
#define SPI_1_DELAYUNITS "ns"
#define SPI_1_EXTRADELAY 0
#define SPI_1_INSERT_SYNC 0
#define SPI_1_IRQ 5
#define SPI_1_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SPI_1_ISMASTER 1
#define SPI_1_LSBFIRST 0
#define SPI_1_NAME "/dev/spi_1"
#define SPI_1_NUMSLAVES 1
#define SPI_1_PREFIX "spi_"
#define SPI_1_SPAN 32
#define SPI_1_SYNC_REG_DEPTH 2
#define SPI_1_TARGETCLOCK 1000000u
#define SPI_1_TARGETSSDELAY "0.0"
#define SPI_1_TYPE "altera_avalon_spi"


/*
 * sys_clk_timer configuration
 *
 */

#define ALT_MODULE_CLASS_sys_clk_timer altera_avalon_timer
#define SYS_CLK_TIMER_ALWAYS_RUN 0
#define SYS_CLK_TIMER_BASE 0x16140
#define SYS_CLK_TIMER_COUNTER_SIZE 32
#define SYS_CLK_TIMER_FIXED_PERIOD 0
#define SYS_CLK_TIMER_FREQ 72000000
#define SYS_CLK_TIMER_IRQ 1
#define SYS_CLK_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYS_CLK_TIMER_LOAD_VALUE 719999
#define SYS_CLK_TIMER_MULT 0.001
#define SYS_CLK_TIMER_NAME "/dev/sys_clk_timer"
#define SYS_CLK_TIMER_PERIOD 10
#define SYS_CLK_TIMER_PERIOD_UNITS "ms"
#define SYS_CLK_TIMER_RESET_OUTPUT 0
#define SYS_CLK_TIMER_SNAPSHOT 1
#define SYS_CLK_TIMER_SPAN 32
#define SYS_CLK_TIMER_TICKS_PER_SEC 100
#define SYS_CLK_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define SYS_CLK_TIMER_TYPE "altera_avalon_timer"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x16180
#define SYSID_ID 0
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1585755836
#define SYSID_TYPE "altera_avalon_sysid_qsys"

#endif /* __SYSTEM_H_ */
