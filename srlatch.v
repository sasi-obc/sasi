module srlatch (s, r, clear, clock, q);
	input s;
	input r;
	input clear;
	input clock;
	output reg q;
	
	wire q_wire;
	assign q_wire = q;

	always @ (posedge clock) begin
		if (clear)
			q = 0;
		else begin
			case ({s, r})
				2'b00: q <= q_wire;
				2'b01: q <= 1'b0;	
				2'b10: q <= 1'b1;
				2'b11: q <= 1'b0;
			endcase
		end
	end
	 
endmodule 