library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Set Generic g_CLKS_PER_BIT as follows:
-- g_CLKS_PER_BIT = (Frequency of i_Clk)/(Frequency of CAN)
-- Example: 10 MHz Clock, 115200 baud CAN
-- (10000000)/(115200) = 87
--This code is for a data size of one Byte
-- (50 000 000)/(450450) = 111

entity CAN2 is
    generic (
    g_CLKS_PER_BIT : integer := 160     
    ); 
port (
    i_clk           : in  std_logic;
    i_TX_DV         : in  std_logic;
    i_TX_Byte       : in  std_logic_vector(7 downto 0);
    i_RX_serial     : in  std_logic;
	 i_reset			  : in  std_logic;
    o_RX_DV         : out std_logic;
    o_RX_Byte       : out std_logic_vector(7 downto 0);
    o_TX_Active     : out std_logic;
    o_TX_Serial     : out std_logic;
    o_TX_Done       : out std_logic;
	 CRC		 		  : in  std_logic_vector(15 downto 0);
	 data_debug : out std_logic
);
end CAN2;

architecture CAN_arch of CAN2 is 
    type t_SM_Main is (s_Idle, s_RX_Start_Bit,s_TX_Start_Bit,s_RX_ID,s_TX_ID,s_TX_ID_DOM,s_RX_Control,s_TX_Control, s_RX_Data_Bits, s_TX_Data_Bits,s_RX_CRC,s_TX_CRC, s_RX_ACK, s_TX_ACK, s_RX_Stop_Bit,s_TX_Stop_Bit, s_Cleanup, s_Cleanup_TX);

    signal r_SM_Main       : t_SM_Main := s_Idle;
    signal r_RX_Data       : std_logic := '0';
    signal r_Clk_Count     : integer range 0 to g_CLKS_PER_BIT-1 := 0;
    signal r_TX_Data       : std_logic_vector(7 downto 0) := (others => '0');
    signal r_RX_Byte       : std_logic_vector(7 downto 0)  := (others => '0');
    signal r_TX_ID         : std_logic_vector(11 downto 0) := "010100011001";
    signal r_RX_Control    : std_logic_vector(6 downto 0)  := (others => '0');
    signal r_TX_Control    : std_logic_vector(6 downto 0)  := "1010000";
    signal r_RX_CRC        : std_logic_vector(15 downto 0) := (others => '0');
	 signal r_TX_CRC        : std_logic_vector(15 downto 0);-- := "1010101010101010";
    signal r_RX_EOF        : std_logic_vector(6 downto 0)  := (others => '0');
	 signal r_TX_EOF        : std_logic_vector(6 downto 0)  := (others => '1');
	 signal r_RX_ACK        : std_logic_vector(1 downto 0)  := (others => '0');
	 signal r_TX_ACK        : std_logic_vector(1 downto 0)  := "10";
    signal r_RX_DV         : std_logic := '0';
	 signal r_TX_Done       : std_logic := '0';
	 signal r_sync    	   : integer   := 0;
	
begin
	 r_TX_CRC <= CRC;--"0010111011001101";
    p_SAMPLE : process (i_Clk)
    begin
		if rising_edge(i_Clk) then
			r_RX_Data <= i_RX_Serial;
      end if;
    end process p_SAMPLE;
	   
    -- Purpose: Control RX state machine
    p_CAN_RX : process (i_Clk)
	 
	 variable r_RX_ID       : std_logic_vector(10 downto 0) := (others => '0');
	 variable r_Bit_Index   : integer := 0; 
	
    begin
			
      if rising_edge(i_Clk) then
			-- Reset, reset state machine to idle and reset variables
			if i_reset= '1' then
				o_RX_Byte <= "00000000";
				r_SM_Main <= s_Idle;
				r_TX_ID  <= "010111011001";
				r_TX_Control <= "1010000";
				r_TX_ACK <= "10";
			end if;

         case r_SM_Main is
   
			-- Idle state (default), reset variables, check if transmitting
         when s_Idle =>
            r_RX_DV     <='0';			-- Drive Rx line low
            r_Clk_Count <= 0;				-- Reset clock count
            r_Bit_Index := 0;				-- Reset bit index
            o_TX_Active <='0';			-- Set tx active to 0
            o_TX_Serial <='1';         -- Drive TX line High
				if r_sync = 16 then
					r_TX_Done   <='0';			-- Set Tx Done flag to 0
				end if;
            r_sync <= r_sync + 1;		-- Increment sync signal to keep track of how long state machine has been waiting in idle
				
				-- enter if trying to transmit and recieving data, wait 50 clock cycles to stay in sync
            if i_TX_DV ='1' and r_sync >= 50 and not r_RX_Data = '1' then
					r_SM_Main <= s_TX_Start_Bit;
					r_TX_Data <= i_TX_Byte;
					r_sync <= 0;
				  
				-- enter if trying to transmit after waiting 1776 clock cycles (maximum possible 1's in a row) to stay in sync
				elsif i_TX_DV ='1' and r_sync >= 1776 then
					r_SM_Main <= s_TX_Start_Bit;
					r_TX_Data <= i_TX_Byte;
					r_sync <= 0;
				
				-- enter if recieving data while not trying to transmit
            elsif r_RX_Data = '0' and  i_TX_DV ='0' then      
					r_SM_Main <= s_RX_Start_Bit;
					r_Clk_Count <= r_Clk_Count + 1;
					r_sync <= 0;
					
				-- otherwise, stay in idle
            else
					r_SM_Main <= s_Idle;
            end if;
   
			-- Transmit start bit
         when s_TX_Start_Bit =>
				o_TX_Active <= '1';
				o_TX_Serial <= '0';
				
				if r_Clk_Count <= g_CLKS_PER_BIT-1 then
				   r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_Start_Bit;
				else
					r_Clk_Count <= 0;
					r_Bit_Index := 0;
					r_SM_Main   <= s_TX_ID;
				end if;
			 
          -- Recieve start bit
         when s_RX_Start_Bit =>
				if r_Clk_Count <= g_CLKS_PER_BIT-1 then
				   r_Clk_Count <= r_Clk_Count + 1;
				   r_SM_Main   <= s_RX_Start_Bit;
				else
				   r_Clk_Count <= 0;
				   r_Bit_Index := 0;
				   r_SM_Main   <= s_RX_ID;
				end if;

			-- Check ID priority
         when s_TX_ID => 
				o_TX_Serial <= r_TX_ID(r_Bit_Index);
            if r_Clk_Count <= g_CLKS_PER_BIT-1 then
               r_Clk_Count <= r_Clk_Count + 1;
               r_SM_Main   <= s_TX_ID;
            else
               r_Clk_Count          <= 0;
               r_RX_ID(r_Bit_Index) := r_RX_Data; -- Read first bit of recieving ID
						
					-- Enter if recieving is dominant
					if (r_TX_ID(r_Bit_Index) = '1' and r_RX_ID(r_Bit_Index)= '0') then --
						if r_Bit_Index < 11 then
							r_SM_Main   <= s_RX_ID; -- go to recessive case
							r_Bit_Index := r_Bit_Index + 1;
						else 
							r_SM_Main   <= s_TX_ID;
						end if;
						
					-- Enter if transmitting is dominant
					elsif (r_TX_ID(r_Bit_Index) = '0' and r_RX_ID(r_Bit_Index) = '1') then 
						if r_Bit_Index < 11 then
							r_SM_Main   <= s_TX_ID_DOM; -- go to dominant case
							r_Bit_Index := r_Bit_Index + 1;
						else 
							r_SM_Main   <= s_TX_ID;
						end if;
						
					-- Otherwise, loop back until there is a difference in ID's
					else
						if r_Bit_Index < 11 then
							r_Bit_Index := r_Bit_Index + 1;
							r_SM_Main   <= s_TX_ID;
						else
							r_Bit_Index := 0;
							r_SM_Main   <= s_TX_Control;
						end if;
					end if;
				end if;
			
			-- "Only transmitting" case
			when s_TX_ID_DOM =>
				o_TX_Serial <= r_TX_ID(r_Bit_Index);
				if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_ID_DOM;
				else
               r_Clk_Count <= 0;
						
					if r_Bit_Index < 11 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_TX_ID_DOM;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_TX_Control;
					end if;
				end if;
			 		
			-- "Only recieving" case		
         when s_RX_ID =>				
				if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_RX_ID;
				else
					r_Clk_Count <= 0;
				
					if r_Bit_Index < 11 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_RX_ID;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_RX_Control;
					end if;
				end if;
				
			-- Transmit control bits
         when s_TX_Control =>
            o_TX_Serial <= r_TX_Control(r_Bit_Index);
				if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_Control;
				else
					r_Clk_Count <= 0;
			 
					if r_Bit_Index < 6 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_TX_Control;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_TX_Data_bits;
					end if;
				end if;
				
         --Read Control bits--
         when s_RX_Control =>
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
              r_Clk_Count <= r_Clk_Count + 1;
              r_SM_Main   <= s_RX_Control;
            else
              r_Clk_Count            <= 0;
              r_RX_Control(r_Bit_Index) <= r_RX_Data;
           
					-- Check if we have received all bits
					if r_Bit_Index < 6 then
					  r_Bit_Index := r_Bit_Index + 1;
					  r_SM_Main   <= s_RX_Control;
					else
					  r_Bit_Index := 0;
					  r_SM_Main   <= s_RX_Data_bits;
					end if;
				end if;          

			 -- Transmit data
          when s_TX_Data_Bits =>
            o_TX_Serial <= r_TX_Data(7 - r_Bit_Index);
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_Data_Bits;
            else
					r_Clk_Count <= 0;
					 
					if r_Bit_Index < 7 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_TX_Data_Bits;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_TX_CRC;
					end if;
				end if;
          
          -- Read data
          when s_RX_Data_Bits =>
			 data_debug <= '1';
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_RX_Data_Bits;
					if r_Clk_Count = g_CLKS_PER_BIT / 2 then
						r_RX_Byte(7 - r_Bit_Index) <= r_RX_Data;
					end if;
            else
					r_Clk_Count            <= 0;
					
						
					-- Check if we have received all bits
					if r_Bit_Index < 7 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_RX_Data_Bits;
					else
						r_Bit_Index := 0;
						r_RX_DV   <= '1';
						r_SM_Main <= s_RX_CRC;
					end if;
				end if;

          -- Read CRC
          when s_RX_CRC => 
			 data_debug <= '0';
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_RX_CRC;
            else
					r_Clk_Count <= 0;
             
					if r_Bit_Index < 15 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_RX_CRC;
					else
					  r_Bit_Index := 0;
					  r_SM_Main   <= s_RX_ACK;
					end if;
				end if;
	
			 -- Transmit CRC
          when s_TX_CRC =>
            o_TX_Serial <= r_TX_CRC(15 - r_Bit_Index);
           
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_CRC;
            else
					r_Clk_Count <= 0;
					 
					if r_Bit_Index < 15 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_TX_CRC;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_TX_ACK;
					end if;
				end if;

          --Read ACK bits
          when s_RX_ACK =>
				o_TX_Serial <= r_TX_ACK(r_Bit_Index);
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_RX_ACK;
            else
					r_Clk_Count            <= 0;
					r_RX_ACK(r_Bit_Index) <= r_RX_Data;
           
					if r_Bit_Index < 1 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_RX_ACK;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_RX_Stop_Bit;
					end if;
				end if;
  
			 -- Transmit ACK bits
          when s_TX_ACK =>
            o_TX_Serial <= r_TX_ACK(r_Bit_Index);
           
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_ACK;
            else
					r_Clk_Count <= 0;
             
					if r_Bit_Index < 1 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_TX_ACK;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_TX_Stop_Bit;
					end if;
				end if;
          
			 -- Read end of frame
          when s_RX_Stop_Bit =>
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_RX_Stop_Bit;
            else
					r_Clk_Count           <= 0;
					r_RX_EOF(r_Bit_Index) <= r_RX_Data;
             
					if r_Bit_Index < 6 then
						r_Bit_Index := r_Bit_Index + 1;
						r_SM_Main   <= s_RX_Stop_Bit;
					else
						r_Bit_Index := 0;
						r_SM_Main   <= s_Cleanup;
					end if;
				end if;

			 -- Transmit end of frame
          when s_TX_Stop_Bit =>
            o_TX_Serial <= r_TX_EOF(r_Bit_Index);
           
            if r_Clk_Count < g_CLKS_PER_BIT-1 then
					r_Clk_Count <= r_Clk_Count + 1;
					r_SM_Main   <= s_TX_Stop_Bit;
            else
					r_Clk_Count <= 0;
             
            if r_Bit_Index < 6 then
					r_Bit_Index := r_Bit_Index + 1;
					r_SM_Main   <= s_TX_Stop_Bit;
            else
					r_Bit_Index := 0;
					r_SM_Main   <= s_Cleanup_TX;
            end if;
          end if;
                     
          -- Stay here 1 clock
          when s_Cleanup =>
            r_SM_Main <= s_Idle;
            r_RX_DV   <= '0';

          -- Stay here 1 clock
          when s_Cleanup_TX=>
            r_SM_Main <= s_Idle;
            r_TX_Done <= '1';
 
			 end case;
      end if;
		
		if r_RX_DV = '1' then
			o_RX_Byte <= r_RX_Byte;
--		else
--			o_RX_Byte <= (others => '0');
		end if;
		
   end process p_CAN_RX;
   
   o_RX_DV   <= r_RX_DV;
	o_TX_Done <= r_TX_Done;
     
 end CAN_arch;
