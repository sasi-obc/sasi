clear; clc; close all;

col = 144;
row = 112;

% col = 128;
% row = 110;
% filename = 'pic';
% file = fopen(filename);
% 
% rawData = fread(file, row * col, 'uint8');


s = serialport("COM6", 115200);
configureTerminator(s,13);

while(1)
        %writeline(s, "python capture");
        num = s.NumBytesAvailable;
        while(num == 0) 
            num = s.NumBytesAvailable;
        end
        rawData = read(s, 16383, "uint8");

        rawData = rawData(1:(col * row));
        bayer = (uint8(reshape(rawData, col, row))).'; 

        I = demosaic(bayer, 'gbrg');
        close all;
        f = figure();
        %f.WindowState = 'maximized';
        imshow(I);
        pause(3);
end
