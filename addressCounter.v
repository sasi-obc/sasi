 module addressCounter(sync, trigger0in, clock, niosCountEnable, niosReset, imageWrite, imageData, trigger0out, address, addressStatus, lineLength);
	input [9:0]sync;
	input trigger0in;
	input clock;
	input niosCountEnable;
	input niosReset;
	output imageWrite;
	output imageData;
	output trigger0out;
	output [13:0]address;
	output [13:0]addressStatus;
	output [11:0]lineLength;
	
	reg imageMem_cs;
	reg imageMem_clken;
	reg imageMem_wren;
	reg [14:0]count;
	reg [5:0]timer;
	reg [11:0]lineLength;
	
	wire countStop;
	wire timerExpired;
	wire q;
	wire reset_trigger;
	wire triggerout_wire;
	wire trigger_buf;
	wire imageData_wire;
	wire frameStart;
	wire frameEnd;
	wire lineLength0;
	wire lineEnd;
	wire done;
	
	assign imageData_wire = (sync == 10'h35);
	assign frameStart = (sync == 10'h22A) & niosCountEnable & ~done;
	assign frameEnd = (sync == 10'h3AA);
	assign lineEnd = (sync == 10'h12A) & q;
	
	assign address = count[13:0];
	assign addressStatus = count[13:0];
	assign countStop = count[14] | (frameEnd & q);
	assign timerExpired = timer[5];
	assign reset_trigger = timer[4];
	assign imageWrite = q;

	assign imageData = imageData_wire;
	assign trigger0out = triggerout_wire;
	assign lineLength0 = (lineLength == 0);
	
	srlatch monitorLatch(frameStart, countStop, niosReset, clock, q);
	srlatch doneLatch(countStop, niosReset, niosReset, clock, done);
	srlatch triggerBuf(trigger0in, reset_trigger, niosReset, clock, trigger_buf);
	srlatch triggerLatch(trigger_buf, timerExpired, niosReset, clock, triggerout_wire);
	
	always @ (posedge clock or posedge niosReset) begin
		if (niosReset) begin
			count <= 0;
			lineLength <= 0;
		end
		else if (lineLength0 && lineEnd)
			lineLength = count[11:0];
		else if (q && imageData_wire && niosCountEnable)
			count <= count + 1;
		else
			count <= count;
	end
	
	always @ (posedge clock or posedge niosReset) begin
		if (niosReset)
			timer <= 0;
		else if (triggerout_wire)
			timer <= timer + 1;
		else 
			timer <= 0;
	end
	
	
endmodule 