
module processor (
	can_reg_clk_in_clk_clk,
	can_reg_s2_address,
	can_reg_s2_chipselect,
	can_reg_s2_clken,
	can_reg_s2_write,
	can_reg_s2_readdata,
	can_reg_s2_writedata,
	can_reg_s2_byteenable,
	clk_clk,
	clk360mhz_se_clk,
	clk72mhz_clk,
	epcs_controller_dclk,
	epcs_controller_sce,
	epcs_controller_sdo,
	epcs_controller_data0,
	gpoutput_export,
	image_data_mem_s2_address,
	image_data_mem_s2_chipselect,
	image_data_mem_s2_clken,
	image_data_mem_s2_write,
	image_data_mem_s2_readdata,
	image_data_mem_s2_writedata,
	image_mem_clk_in_clk_clk,
	python_status_address,
	python_status_chipselect,
	python_status_clken,
	python_status_write,
	python_status_readdata,
	python_status_writedata,
	python_status_byteenable,
	reset_reset_n,
	spi_0_MISO,
	spi_0_MOSI,
	spi_0_SCLK,
	spi_0_SS_n,
	spi_1_MISO,
	spi_1_MOSI,
	spi_1_SCLK,
	spi_1_SS_n,
	uart_rxd,
	uart_txd,
	nios_pll_reset_export);	

	input		can_reg_clk_in_clk_clk;
	input	[2:0]	can_reg_s2_address;
	input		can_reg_s2_chipselect;
	input		can_reg_s2_clken;
	input		can_reg_s2_write;
	output	[31:0]	can_reg_s2_readdata;
	input	[31:0]	can_reg_s2_writedata;
	input	[3:0]	can_reg_s2_byteenable;
	input		clk_clk;
	output		clk360mhz_se_clk;
	output		clk72mhz_clk;
	output		epcs_controller_dclk;
	output		epcs_controller_sce;
	output		epcs_controller_sdo;
	input		epcs_controller_data0;
	output	[12:0]	gpoutput_export;
	input	[13:0]	image_data_mem_s2_address;
	input		image_data_mem_s2_chipselect;
	input		image_data_mem_s2_clken;
	input		image_data_mem_s2_write;
	output	[7:0]	image_data_mem_s2_readdata;
	input	[7:0]	image_data_mem_s2_writedata;
	input		image_mem_clk_in_clk_clk;
	input	[1:0]	python_status_address;
	input		python_status_chipselect;
	input		python_status_clken;
	input		python_status_write;
	output	[31:0]	python_status_readdata;
	input	[31:0]	python_status_writedata;
	input	[3:0]	python_status_byteenable;
	input		reset_reset_n;
	input		spi_0_MISO;
	output		spi_0_MOSI;
	output		spi_0_SCLK;
	output		spi_0_SS_n;
	input		spi_1_MISO;
	output		spi_1_MOSI;
	output		spi_1_SCLK;
	output		spi_1_SS_n;
	input		uart_rxd;
	output		uart_txd;
	input		nios_pll_reset_export;
endmodule
