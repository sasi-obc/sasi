module CANMemoryController(clk, RX_serial, TX_serial, memory_clken, memory_wren, memory_cs, memory_byteen, address, memoryReadData, memoryWriteData);
	input clk;
	input RX_serial;
	input [31:0]memoryReadData;
	output TX_serial;
	output memory_clken;
	output memory_cs;
	output reg memory_wren;
	output [3:0]memory_byteen;
	output [2:0]address;
	output reg [31:0]memoryWriteData;

	reg [31:0]memory_register;
	reg [31:0]memory_register_temp;
	reg [7:0]RX_Data;
	reg [1:0]state;
	
	wire TX_DV;
	wire reset;
	wire [7:0]TX_Byte;
	wire RX_DV;
	wire TX_Active;
	wire TX_serial_wire;
	wire TX_Done;
	wire [7:0]RX_Byte;
	wire [15:0]CRC;
	
	wire data_debug_wire;
	
	assign address = 3'b000;
	assign memory_byteen = 4'b1111;
	assign memory_cs = 1;
	assign memory_clken = 1;
	
	assign TX_DV = memory_register[0];
	assign TX_Byte = memory_register[23:16];
	assign reset = memoryReadData[1];
	
	assign TX_serial = TX_serial_wire;

	CAN2 can(
				clk,
				TX_DV,
				TX_Byte,
				RX_serial,
				reset,
				RX_DV,
				RX_Byte,
				TX_Active,
				TX_serial_wire,
				TX_Done,
				CRC,
				data_debug_wire
				);
		defparam	can.g_CLKS_PER_BIT = 160;
		
	CRCGenerator CRCGenerator0(clk, TX_Byte, CRC);
		
	always @ (RX_Byte)
		RX_Data <= RX_Byte;
		
	always @ (posedge clk) begin
		if(reset)
			state <= 0;
		else begin
			case(state)
				0 : 
				begin
					memory_wren <= 0;
					memory_register <= memoryReadData;
					memoryWriteData <= memoryReadData;
					memory_register_temp <= memoryReadData;
					state <= 1;
			   end
				
				1 : 
				begin
					if(TX_Active & TX_DV)
						memory_register_temp[0] <= 0;
					if(TX_Done)
						memory_register_temp[2] <= TX_Done;
					if(RX_DV)
						memory_register_temp[3] <= RX_DV;
					memory_register_temp[15:8] <= RX_Data;
					state <= 2;
				end
				
				2 :
				begin
					if(memory_register != memory_register_temp) begin
						memory_wren <= 1;
						memoryWriteData[0] <= memory_register_temp[0];
						if(memory_register_temp[2])
							memoryWriteData[2] <= memory_register_temp[2];
						if(memory_register_temp[3])
							memoryWriteData[3] <= memory_register_temp[3];
						memoryWriteData[15:8] <= RX_Data;
					end
					state <= 0;
				end
			endcase	
		end
	end
endmodule
