library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--Function takes in data and ID and applies CAN protocol
--need to implement part that checks whether a high priority node is using the bus (I.E. check for a 1 on the bus)


entity CRC_Decoder is
    port 
    (
		i_Code 			: in std_logic_vector(17 downto 0);
		i_clk				: in std_logic;
		o_remainder	   : out std_logic_vector(10 downto 0)
    );
end CRC_Decoder;

architecture behaviour of CRC_Decoder is
begin

process(i_clk)
 variable r_CRC_Poly		:std_logic_vector(10 downto 0) := "11001001111"; -- Polynomial used to check and encode
 variable w		  			:std_logic_vector(10 downto 0);
 variable y		 			:std_logic_vector(10 downto 0);
 variable i,j    			:integer:=0;
 
 begin
 
 w := i_code(17 downto 7); -- take the first 11 bits of r_Code
 
 for i in 7 downto 0 loop -- if msb of w is 1, xor that ish with the polynomial
		if(w(10)='1') then
			w:=w xor r_CRC_Poly; -- xor the first 11 bits of r_Code with the crc polynomials
		else
			null;
		end if;
		
		if(i > 0) then
			y:=w; -- y gets the result
			
			w(10 downto 1):=y(9 downto 0); --shift once to the left
			
			if(i=0) then
				w(0):='0';
			else
				w(0):=i_code(i-1); -- fill in the last bit with the next bit in r_Code
			end if;
		end if;
 end loop;
 
o_remainder(10 downto 0)<= w(10 downto 0); -- first 8 bits is the original data
 end process;

end behaviour;
